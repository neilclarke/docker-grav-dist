---
title: ЮНЕСКО Специалист по подготовке проекта
menu: Главная страница
slug: Главная страница
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---

# Мы разработали «Основные рекомендации для молодых активистов», чтобы помочь вам воплотить ваши идеи в виде конкретного плана мероприятий