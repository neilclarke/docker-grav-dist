---
title: 项目规划者
menu: 首页
slug: home
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---

# 我们编写《青年行动攻略》，目的就是协助各位青年小伙伴把想法变成切实可行的项目计划。