---
title: Homepage Features
menu: Getting started
features:
    - title: Your big idea pinned down
      text: If you have identified a problem in your community, and have an idea of how it can be solved or addressed, then you can carry out a project.
      image: home-feature-one
    - title: Tips and resources throughout your project
      text: We’ve brought together tools, information, and additional resources, and put them in one place to make it easier for you to design and implement projects focused on generating social impact.
      image: home-feature-two
    - title: A co-created master plan
      text: At the end, you will have drafted your project plan. So, basically, you bring the ideas, and we help you piece everything together. Ready?
      image: home-feature-three
---