---
title: 'Homepage Features'
menu: 'Getting started'
features:
    -
        title: 'Tu gran idea, definida.'
        text: 'Si has detectado un problema en tu comunidad y tienes una idea sobre cómo resolverlo o abordarlo, puedes llevar a cabo un proyecto.'
        image: home-feature-one
    -
        title: 'Consejos y recursos durante tu proyecto'
        text: 'Hemos reunido en un solo lugar instrumentos, información y recursos adicionales para que te resulte más fácil diseñar y ejecutar proyectos centrados en generar un impacto social.'
        image: home-feature-two
    -
        title: 'Un plan director colaborativo'
        text: 'Al final, habrás redactado tu plan de proyecto. Esencialmente, tú aportas las ideas, y nosotros te ayudamos a estructurarlas. ¿Comenzamos?'
        image: home-feature-three
---

