---
title: 'Homepage Features'
menu: 'Getting started'
features:
    -
        title: 'Ta grande idée, définie'
        text: 'Si vous constatez un problème dans votre communauté et que vous avez une idée sur la façon de le résoudre ou d’y faire face, vous pouvez vous aussi réaliser un projet.'
        image: home-feature-one
    -
        title: 'Conseils et ressources pendant ton projet'
        text: 'Nous avons réuni ici des outils, des informations et d’autres ressources pour vous aider à concevoir et à mettre en œuvre des projets visant à produire un impact social.'
        image: home-feature-two
    -
        title: 'Un plan directeur co-créé'
        text: "À la fin du processus, vous aurez rédigé votre plan de projet. Pour faire simple, vous venez avec vos idées, et nous vous aidons à tout mettre en place. Vous êtes prêts\_?"
        image: home-feature-three
---

