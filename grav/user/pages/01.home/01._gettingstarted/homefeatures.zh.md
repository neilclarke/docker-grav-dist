---
title: 'Homepage Features'
menu: 'Getting started'
features:
    -
        title: 项目核心思想
        text: 当你发现社区存在问题，想要设计一个项目解决它，却不知如何入手？
        image: home-feature-one
    -
        title: 项目建议与资源
        text: '具体方法如下: 我们把工具、信息和补充资源集中起来，方便大家从零开始，规划和实施一个产生社会效益的项目。'
        image: home-feature-two
    -
        title: 共同管控计划
        text: 你最终将拟定自己的项目规划。简单说来，就是你提出想法，我们帮你落实细节。准备好了吗？
        image: home-feature-three
---
