---
title: Les Conseils pratiques pour l’action des jeunes, comment ça marche ?
chartimage: phase-chart-sample.png
---
Les Conseils pratiques pour l’action des jeunes sont structurés en six sections qui retracent le cycle du projet et correspondent à ses différentes étapes. Dans chaque section, vous trouverez des informations indispensables et des points dont il faudra tenir compte lorsque vous examinerez les questions d’orientation et de réflexion qui vous seront posées. Enfin, à la fin de chaque étape, des liens vous permettront de découvrir d’autres ressources, outils et directives et d’approfondir ainsi votre action.