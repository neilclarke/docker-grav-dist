---
title: مخطط مشروع اليونسكو
menu: الصفحة الرئيسية
slug: home
template: home
headerimage: home-header.svg
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---

# قد وضعنا نصائح للشباب لمساعدتك على تحويل فكرة الى خطة عمل فعلية للمشروع.
