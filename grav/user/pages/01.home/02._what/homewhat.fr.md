---
title: Qu’est-ce qu’un projet?
---
### On peut définir un projet comme une entreprise temporaire, une série de tâches menées en vue d’atteindre des objectifs précis.

Les projets sont restreints par la limitation des ressources et ont une durée précise : ils ont donc un début et une fin.