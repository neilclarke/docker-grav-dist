---
title: 'Innovating in Project Management'
menu: Management
visible: false
---

Project management can also tap onto a range of alternative (“non-traditional”) approaches, methodologies and tools in order to best address the problem identified and to better achieve project objectives. Through various innovation mechanisms, we can try to find more effective solutions to different social challenges. Below are three ideas that you can consider when thinking about your project.

## Human-centered design

When we initially design a project we tend to look at a problem from our own perspective. Human-centered design puts the end-user or direct beneficiary at the center of the project. It is focused on truly finding out what their needs are, generating empathy and developing solutions that address those needs. This iterative process tends to lead to better informed solutions and helps builds trust. It can be a way to improve the design of your project and build links with the community where you want 

## Testing & prototyping

In projects, we should allow space for testing and learning, before fully committing time and financial resources to one activity. Because, what if it doesn’t work? This means, that you might want to test a particular activity on a small scale to check the results, then learn from this and scale-up. You can also test different alternatives, to save time and resources.  

## Crowdfunding

An online fundraising mechanism that taps on the “crowd” to raise funds for diverse initiatives. Running a successful crowdfunding campaign takes time and effort. However, this might be an interesting option to consider in order to raise funds for your project.

<section id="section_learnmore" class="question">
	<h2 id="_learnmore">Want to learn more?</h2>
		<div class="section-body">
        <p>Here are two comprehensive project management tools that could help you organize and manage your project:</p>
		<ul>
			<li><a target="_blank" href="https://asana.com/try?noredirect">Asana</a><br>
			Collaborative and free online project organizer</li>
			<li><a href="https://products.office.com/fr-ca/project/project-and-portfolio-management-software?tab=tabs-1" target="_blank">Microsoft Project</a><br>
			Complete project management tool</li>
		</ul>
		</div>
</section>