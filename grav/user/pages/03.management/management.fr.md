---
title: Innover dans la gestion de projet
menu: Gestion de projet
visible: false
---
La gestion de projet peut également s’inspirer d’un grand nombre d’approches, de méthodes et d’outils moins classiques afin de régler au mieux le problème constaté et d’atteindre plus efficacement les objectifs du projet. Grâce à divers mécanismes innovants, nous pouvons essayer d’apporter des solutions plus efficaces pour traiter divers problèmes sociaux. Vous trouverez ci-dessous trois idées qui vous donneront matière à réflexion lors de l’élaboration de votre projet.

## Conception axée sur l’être humain

Lorsque nous commençons à concevoir un projet, nous avons tendance à considérer le problème selon notre propre point de vue. La conception axée sur l’être humain place l’utilisateur final ou le bénéficiaire direct au centre du projet. Elle a pour objet de déterminer réellement quels sont leurs besoins, d’encourager l’empathie et d’élaborer des solutions pour répondre à ces besoins. Ce processus itératif fournit généralement des solutions plus éclairées et contribue à renforcer la confiance. Il peut vous aider à améliorer l’élaboration de votre projet et à établir des liens avec la communauté dans laquelle vous souhaitez travailler. 

## Réaliser des essais et des prototypes

Lorsque l’on réalise un projet, il serait bon de donner plus d’importance aux essais et à l’apprentissage avant de consacrer du temps et des ressources financières à une activité particulière. En effet, que faire si cette activité ne marche pas ? Autrement dit, mieux vaut tester une activité donnée sur une petite échelle pour vérifier les résultats obtenus et en tirer des enseignements avant de la transposer à une plus grande échelle. Pour économiser du temps et des ressources, vous pouvez également tester plusieurs solutions.

## Crowdfunding

Mécanisme collaboratif en ligne de collecte de fonds qui puise dans la “foule” (crowd) afin de financer diverses initiatives. Une campagne réussie de crowdfunding exige qu’on y consacre du temps et des efforts. Ce mécanisme peut toutefois être un moyen intéressant de collecter des fonds pour financer votre projet.

Pour en savoir plus! Voici deux outils exhaustifs de gestion de projet susceptibles de vous aider à organiser et gérer votre projet:

<section id="section_learnmore" class="question">
	<h2 id="_learnmore">Pour en savoir plus!</h2>
		<div class="section-body">
		<ul>
			<li><a target="_blank" href="https://asana.com/try?noredirect">Asana</a><br>
			Outil en ligne et gratuit de gestion de projet collaborative</li>
			<li><a href="https://products.office.com/fr-ca/project/project-and-portfolio-management-software?tab=tabs-1" target="_blank">Microsoft Project</a><br>
			Outil complet de gestion de projet</li>
		</ul>
		</div>
</section>