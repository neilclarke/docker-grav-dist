---
title: 项目管理创新
menu: 项目管理创新
visible: false
---
项目管理还可以采用多种“非传统”方式、方法和工具，改善项目成果。我们可以借助多种创新机制，为社会难题寻求更加有效的解决方案。以下提出三种设想，供你参考。

## 以人为本的设计方案

W在最初设计项目时，我们往往会从自身角度来看待问题。以人为本的设计方案则是将终端用户或直接受益者作为项目核心，着眼于切实了解这些人的需求，对其感同身受，而后制定解决办法，满足他们的需求。这一迭代过程往往会让解决方案更加睿智，并且有助于增进信任。可以采用这种方法来改善方案设计，与工作所在的社区建立联系。 

## 项目测试

我们应在项目中为测试和学习预留空间，而后再集中时间和资源用于开展活动。假如当前工作失败，我们该怎么办？这意味着我们先要在小范围内测试某项活动，从中吸取经验教训，然后再推而广之。为节省时间和资源，你还可以测试多种替代方案。  

## 众筹

这是一种在线融资机制，利用公众的力量为各种倡议筹集资金。一个成功的众筹需要付出许多时间和精力。

<section id="section_learnmore" class="question">
	<h2 id="_learnmore">想了解更多？</h2>
		<div class="section-body">
		<ul>
			<li><a target="_blank" href="https://asana.com/try?noredirect">Asana</a><br>
			协作式免费在线项目组织方</li>
			<li><a href="https://products.office.com/fr-ca/project/project-and-portfolio-management-software?tab=tabs-1" target="_blank">微软Project软件</a><br>
			全套项目管理工具</li>
		</ul>
		</div>
</section>