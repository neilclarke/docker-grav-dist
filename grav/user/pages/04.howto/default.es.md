---
title: 'Cómo utilizar'
menu: 'Cómo utilizar'
visible: true
---

Esta página da instrucciones y guía sobre cómo utilizar el Planeador de proyectos de la UNESCO solo o en equipo para aprovechar al máximo todas sus características.

El planeador de proyectos tiene como intención guiarte a través de tu proyecto y tiene como objetivo cubrir tareas y desafíos comunes. Las fases y preguntas dentro del Planeador siguen la estructura típica de un proyecto. Depende de ti si afrontas las fases y sus respectivas preguntas en el orden presentado o si trabajas en ellos en acuerdo a tus necesidades individuales y prioridades.

Cuando veas una página de Fase verás una opción para acceder a una vista previa de tu trabajo, hacer clic en este botón abrirá un panel de Vista Previa; este panel de vista previa te dará una visión de conjunto de todas las preguntas y respuestas que ya has introducido. Aquí puedes:

* Revisar tu plan de proyecto
* Crear un PDF de tu plan de proyecto
* Exportar tu plan como un archivo para guardar y compartir tu trabajo en progreso \*
* Importar un plan previamente exportado para continuar trabajando en el Planeador de Proyectos

\* _Atención: la UNESCO no guarda tus datos personales o el contenido introducido en esta página web en sus servidores. Por ello, por favor siempre asegúrate de exportar tus datos para guardar tu progreso y evitar perder tu trabajo._

Si trabajas en equipo con otras personas y quieres trabajar en tu plan de proyecto a través de varios miembros y dispositivos, sugerimos el uso del [servicio de almacenamiento de archivos](https://en.wikipedia.org/wiki/Comparison_of_file_hosting_services) para tus datos de proyecto TXT para mantener tu contenido sincronizado y accesible para todos. También recuerda hacer una copia de seguridad de tus datos frecuentemente, por ejemplo, en un disco duro externo.

## Cómo exportar tus datos:

1. Introduce el texto en los campos de texto debajo de cada pregunta en el Planeador para escribir tu plan de proyecto.
2. Usa el panel de Vista Previa para ver todas las preguntas y el texto que ya has introducido.
3. Haz clic o presiona en el botón de "Exportar Datos" para generar un archivo TXT que contenga todo tu texto introducido.
4. El archivo TXT será descargado a tu dispositivo y podrá ser guardado localmente en un servicio de alojamiento de archivos para hacerlo accesible para todos.

## Cómo importar tus datos 

1.	En el panel de Vista Previa haz clic o presiona "Importar Proyecto".
2.	Selecciona un archivo TXT previamente generado desde tu dispositivo local o servicio de alojamiento de archivos.
3.	El contenido del archivo TXT será importado en la ventana activa de tu buscador y expuesto en los campos de texto del Planeador de Proyectos.