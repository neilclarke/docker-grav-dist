---
title: 'How to use'
menu: 'How To'
visible: true
---

This page provides instructions and guidelines on how to use the UNESCO Project Planner alone or in a team to take full advantage of its features.

The Project Planner is intended to guide you through your project and aims to cover common tasks and challenges. The phases and questions inside the Planner follow the typical structure of a project. It is up to you to whether you tackle the phases and their questions in the presented order, or work through them based on your individual needs and priorities.

When viewing a Phase page you will see an option to preview your work, clicking on this button will open a Preview panel; this Preview panel gives you an overview of all the questions and answers that you have already entered. Here you can:

* Review your project plan
* Create a PDF of your project plan
* Export your plan as a file for saving and sharing your work in progress. \* 
* Import a previously exported plan to resume working in the Project Planner

\* _Please note: UNESCO doesn't store any of your personal data or the content you enter on this website to their servers. Therefore please always make sure to export your data to save your progress and prevent your work being lost._

If you work in a team with others and want to work on your project plan across different team members and devices we suggest the use of [file hosting services](https://en.wikipedia.org/wiki/Comparison_of_file_hosting_services) for your TXT data project file to keep your content synchronized and accessible for everyone. Also remember to regularly back up your data, e. g. on an external hard drive.

## How to export your data:

1. Enter text into the text fields below each question in the Planner to write your project plan.
2. Use the Preview panel to see all questions and the text you have already entered.
3. Click or tap on “Export Data” button to generate a TXT file containing all of your entered text.
4. The TXT file will be downloaded to your device and can be stored locally or saved to a file hosting service to make it accessible to others. 

## How to import your data:

1. In the Preview panel click or tap on “Import Project”.
2. Select a previously generated TXT file from your local device or file hosting service.
3. The content of the TXT file will be imported into your current browser window and displayed in the text fields of the Project Planner.
4. You can now modify or update any content in the Project Planner to continue creating your project plan