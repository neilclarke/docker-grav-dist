---
title: 'Guide d’ utilisation'
menu: Guide
visible: true
---

Cette page fournit des instructions et des explications quant à la façon d'utiliser le planificateur de projets de l'UNESCO, seul(e) ou en équipe, pour bénéficier pleinement de ses fonctionnalités.

Le planificateur de projets est là pour vous guider à travers votre projet et a pour objectif de couvrir des tâches et défis communs. Les étapes et questions du planificateur sont organisées selon la structure type d'un projet. À vous ensuite de décider si vous voulez appréhender les étapes et leurs questions dans l'ordre présenté ou si vous préférez les traiter selon vos besoins et priorités.

Lorsque vous vous trouvez sur une des pages « Étape », il existe une option vous permettant avoir un aperçu de votre travail : en cliquant sur ce bouton, un panneau « Aperçu » s'ouvre. Vous voyez ainsi toutes les questions ainsi que les réponses que vous avez entrées. Là, vous pouvez :

-	Réviser votre plan de projet
-	Créer un PDF de votre plan de projet
-	Exporter votre plan sous forme de fichier pour le sauvegarder et partager votre travail en cours \* 
-	Importer un plan précédemment exporté pour revenir à votre travail dans le planificateur de projets

\* _Veuillez noter que l'UNESCO ne conserve aucune donnée personnelle ou contenu enregistré sur cette page web dans ses serveurs. Ainsi, veillez à toujours exporter vos données pour sauvegarder votre travail et éviter qu'il ne soit perdu._

Si vous travaillez en équipe et que plusieurs personnes veulent accéder à votre plan de projet via différents appareils, nous vous suggérons d'utiliser un [système d'hébergement de fichiers](https://en.wikipedia.org/wiki/Comparison_of_file_hosting_services) pour que votre fichier TXT, contenant les données du projet, se synchronise et reste accessible à tous. Veillez également à faire des sauvegardes de vos données régulièrement, sur un disque dur externe par exemple.

## Comment exporter vos données :

1. Sous chaque question, entrez du texte dans les champs prévus à cet effet dans le planificateur pour rédiger votre plan de projet.
2. Utilisez le panneau d'aperçu pour voir toutes les questions et texte déjà écrit.
3. Cliquez sur « Exporter les données » pour générer un fichier TXT contenant le texte entré.
4. Le fichier TXT est alors téléchargé sur votre machine et peut être sauvegardé localement ou dans un système d'hébergement de fichiers pour le rendre accessible à d'autres personnes.


## Comment importer vos données :

1. Dans le panneau « Aperçu », cliquez sur « Importer un projet ».
2. Sélectionnez un fichier TXT généré précédemment localement sur votre ordinateur ou sur un service d'hébergement de données.
3. Le contenu du fichier TXT sera importé dans la fenêtre active de votre navigateur et s'affichera dans les champs de texte du planificateur de projets.
4. Désormais, vous pouvez modifier ou mettre à jour tout contenu dans le planificateur de projets pour continuer à créer votre plan de projet.
