---
title: 'Have you defined the activities required to reach the objectives of your project?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Let’s go back to the initial project plan you developed which has specific objectives and results you want to achieve. In this stage, we have to breakdown each objective into manageable actions and activities that you will put in place in order to actually accomplish the expected results. Therefore, for each project objective, you should think of 2–5 key actions that you will do in order to reach that specific objective. Quality beats quantity. This means that it is often better to do two key activities that have significant impact, rather than lots of small activities. Think of activities that will _contribute_ to the change you want to achieve in your project. Furthermore, consider the long-term change you envision, and identify activities that can make these changes sustainable even after the project concludes. During the project implementation, always ask yourself if what you are doing is contributing to the objectives of the project.