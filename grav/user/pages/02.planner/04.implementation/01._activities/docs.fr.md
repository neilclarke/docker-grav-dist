---
title: Avez-vous défini les activités qui vous permettront d’atteindre les objectifs de votre projet ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Revenons au plan initial du projet, dans lequel vous avez défini les objectifs et les résultats que vous souhaitez atteindre. Au cours de cette étape, chaque objectif sera décomposé en tâches et en activités à effectuer afin d’obtenir les résultats attendus. Pour chaque objectif du projet, vous devrez donc sélectionner plusieurs actions clés (entre 2 et 5) en vue d’atteindre l’objectif fixé. Mieux vaut viser la qualité que la quantité. Ce qui signifie qu’il vaut souvent mieux se concentrer sur deux tâches essentielles qui auront un impact significatif plutôt que de se lancer dans un grand nombre d’activités de moindre portée. Choisissez les actions qui contribueront réellement aux changements que vous voulez susciter grâce à votre projet. Ayez en outre à l’esprit le changement à long terme que vous imaginez et prévoyez des actions qui rendront ce changement durable, même une fois le projet achevé. Pendant la mise en œuvre du projet, demandez-vous si ce que vous faites contribue aux objectifs fixés.