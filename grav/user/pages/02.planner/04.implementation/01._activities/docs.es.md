---
title: ¿Has definido las actividades necesarias para alcanzar los objetivos de tu proyecto? 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Volvamos al plan del proyecto inicial que contiene los objetivos y resultados específicos que deseas alcanzar. En esta fase, tenemos que dividir cada objetivo en acciones y actividades manejables que realizarás para alcanzar los resultados deseados. Así, para cada objetivo del proyecto, deberás definir entre dos a cinco acciones esenciales que realizarás para alcanzar ese objetivo. La calidad prima sobre la cantidad, por lo que muchas veces es mejor realizar dos actividades esenciales que tienen un impacto considerable que muchas actividades. Piensa en actividades que contribuirán al cambio que deseas alcanzar en tu proyecto. Además, analiza el cambio a largo plazo que quieres lograr e identifica las actividades que pueden hacer que estos cambios sean sostenibles aún después de concluido el proyecto. Durante la ejecución del proyecto, pregúntate constantemente si lo que haces contribuye a los objetivos del mismo.