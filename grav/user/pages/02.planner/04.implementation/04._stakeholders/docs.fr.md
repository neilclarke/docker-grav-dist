---
title: Quelles sont les autres parties prenantes impliquées ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
La mise en œuvre de votre projet ne sera pas un processus isolé, elle entraînera la participation d’autres parties prenantes, que ce soit la communauté dans laquelle vous travaillez, les donateurs qui financent votre projet ou les pouvoirs publics. Vous veillerez à collaborer avec elles dans l’intérêt du projet. Prenez donc le temps de recenser toutes les parties prenantes, de déterminer quels sont leurs liens avec le projet et de prévoir comment vous travaillerez avec elles pendant toute la durée de la mise en œuvre.