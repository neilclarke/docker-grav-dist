---
title: ¿Es posible realizar las actividades en el tiempo y con el presupuesto establecido?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Para una buena gestión del proyecto, es esencial asegurar un control financiero y una gestión del tiempo adecuados para alcanzar los objetivos del mismo. Ten en cuenta que las actividades que hayas decidido realizar deberían estar dentro del presupuesto de tu proyecto. Una vez que hayas empezado a ejecutar el proyecto, tendrás que asegúrate que los gastos no sobrepasen tu presupuesto. Revisa tu presupuesto constantemente. Además, ¿te acuerdas que calculaste el tiempo necesario para llevar a cabo el proyecto? Ahora tienes que incorporar estas actividades a ese período para crear un cronograma de trabajo. Se debe decidir cuándo se realizará cada una de las actividades y cuánto durarán. Cuando empieces a ejecutar tu proyecto, puedes utilizar este cronograma para monitorear su progreso. Te permitirá saber que se ha llevado a cabo o no se ha hecho y así saber si el proyecto se ejecuta puntualmente o si está retrasado.