---
title: 'Can these activities be carried out within the project’s timeframe and budget?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Ensuring adequate financial control and time management to achieve project objectives is key for good project management. Note that the activities you identified should be within the budget you established for your project. Once you start implementing the project you have to make sure that the expenses are not surpassing your budget. Revisit your budget constantly. Furthermore, remember that you developed an estimated timeframe for implementing your project? Now, you have to incorporate these activities into your timeframe to create a work schedule, deciding when you will carry out each activity and how long they will last. As you start implementing your project, you can use this schedule to verify what has been completed or not, and to check if your project is on time or delayed. This way you will also be able to monitor the progress of your project.