---
title: Ejecución y coordinación del proyecto
subtitle: Fase 4
menu: Ejecución
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Preguntas sobre tu proyecto en la etapa de ejecución y coordinación.