---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías 

-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	Cómo hacer una llamada a actuar por Facebook
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">Gantt Chart</a>       
	¿Qué es un gráfico de Gantt para la programación de proyectos?
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">Microsoft Office</a>   
	Plantillas de cronograma y calendario
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	Plantilla para la organización del equipo y las tareas encomendadas a cada uno de sus miembros
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Mujeres</a>   
	Cómo empezar a ejecutar tu proyecto - págs. 117-120 
-	<a target="_blank" href="http://www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	Plantilla de gráfico de Gantt para Excel
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">Youtube</a>         
	Cómo crear un gráfico de Gantt elemental (tutorial en vídeo)