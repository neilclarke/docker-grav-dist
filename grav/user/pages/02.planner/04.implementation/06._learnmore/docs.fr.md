---
title: Pour en savoir plus!
menu: Pour en savoir plus!
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives 

-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	Comment lancer un appel à l’action sur Facebook
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">Diagramme de Gantt</a>       
	Qu’est-ce qu’un diagramme de Gantt, comment s’en servir pour planifier le projet ?
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">Microsoft Office</a>   
	Modèles de calendriers et d’échéanciers
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	Modèle d’organisation des équipes et de répartition des tâches
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Femmes</a>   
	Comment commencer la mise en œuvre de votre projet – pp. 117-120  
-	<a target="_blank" href="http://www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	Modèle de diagramme de Gantt pour Exce
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">Youtube</a> 
	Comment créer un diagramme de Gantt simple (tutoriel vidéo)