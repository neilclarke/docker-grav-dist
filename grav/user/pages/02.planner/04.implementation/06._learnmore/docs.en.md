---
title: 'Want to learn more?'
taxonomy:
    category:
        - docs
menu: 'Learn More'
slug: _learnmore
hasinlinefield: 'false'
---

-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	How to implement a call for action on Facebook
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">Gantt Chart</a>       
	What is a Gantt chart for project scheduling?
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">Microsoft Office</a>   
	Timeline and schedule templates
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	Team organization and assignments template 
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>   
	How to start delivering your project, pp. 117–120 
-	<a target="_blank" href="http://www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	Gantt chart template for Excel
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">YouTube</a>         
	How to create a basic Gantt chart (Doug H video tutorial)