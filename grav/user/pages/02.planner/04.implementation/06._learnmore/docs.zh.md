---
title: 想了解更多？
menu: 想了解更多？
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	如何在Facebook上发出行动呼吁
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">甘特图</a>       
	什么是项目进度甘特图？
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">微软Office软件</a>   
	时间线和时间表模板
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	团队组织和人员配置模板
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">联合国妇女署</a>   
	《教你如何实施项目》，第117-120页
-	<a target="_blank" href="http://www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	Excel甘特图模板
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">Youtube</a>         
	甘特图入门（教学视频）