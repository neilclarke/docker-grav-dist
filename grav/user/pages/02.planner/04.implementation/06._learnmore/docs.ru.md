---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации.

-  <a target="_blank" href="https://nonprofits.fb.com/topic/ask-people-to-speak-out/#define-actions-to-take">Facebook</a>   
	Как осуществить призыв к действию в Facebook
-  <a href="https://www.youtube.com/watch?v=ADK58IRPKh8" target="_blank">Диаграмма Ганта</a>       
	Что такое диаграмма Ганта, используемая для планирования графика работ по проекту?
-	<a target="_blank" href="https://templates.office.com/en-us/Timelines">Microsoft Office</a>   
	Шаблоны планов-графиков
-	<a target="_blank" href="https://images.template.net/wp-content/uploads/2016/07/27083817/Team-Assignment-Template.doc">Project Connections</a>     
	Шаблон командной организации работы и распределения заданий
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ООН-женщины</a>   
	Как начать осуществление собственного проекта (с. 117-120)
-	<a target="_blank" href="http://www.wikihow.com/Sample/Gantt-Chart">WikiHow</a>         
	Шаблон диаграммы Ганта для Excel
-	<a target="_blank" href="https://www.youtube.com/watch?v=TjxL_hQn5w0">Youtube</a>         
	Как создать простую диаграмму Ганта (обучающий видеоролик)