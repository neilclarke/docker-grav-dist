---
title: Project Fundraising
subtitle: Phase 3
menu: Fundraising
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
Questions about getting the funds needed in order to implement your project.
