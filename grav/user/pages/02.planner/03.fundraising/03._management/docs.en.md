---
title: 'How will you manage the funds?'
taxonomy:
    category:
        - docs
---

If you do get the funds, where are you going to deposit them? Often, this implies that the organization you work with has to be legally registered and have a bank account. If this is not a possibility, another mechanism can include asking donors to directly carry out expenditures (purchase equipment, pay for rent, etc.) for the project. 

If you are working with other (larger) partner organizations they might be able to receive the funds. Keep in mind that perhaps you (or your organization) will be required to sign a contract or agreement with the donor to guarantee that the funds are spent on the project. Read carefully the agreement to make sure that your organization will be able to fulfill all donor expectations. Note that donors might ask for evidence of previous projects implemented, or expertise in managing funds. 

Finally, keep in mind that resource allocation will depend on the donor, and can come before the implementation of the project, after (depending on the results) or an advance can be given at the beginning with the remaining percentage awarded upon completion of the project. 