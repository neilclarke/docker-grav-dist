---
title: جمع التبرعات للمشروع
subtitle: المرحلة الثالثة
menu: التبرعات
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category: docs
---
أسئلة بشأن الحصول على الأموال اللازمة لتنفيذ مشروعك.
