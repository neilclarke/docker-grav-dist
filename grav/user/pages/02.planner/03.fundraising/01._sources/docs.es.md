---
title: ¿Cuáles son las diferentes fuentes de financiación necesarias para ejecutar tu proyecto?
taxonomy:
    category: docs
---
Ahora que tienes un plan de proyecto y un presupuesto es el momento de pensar cómo conseguir los fondos para poner en acción tu proyecto. Por lo tanto, es importante identificar distintas fuentes de financiación, dependiendo de la finalidad y el tema de tu proyecto. Puede haber diversos donantes, entre ellos el sector privado (empresas), fundaciones, y organizaciones internacionales. Además, en tu comunidad pueden existir fuentes de financiación específicas, a través de las autoridades locales y/o nacionales. Procura hacer una búsqueda organizada, empieza localmente (en tu municipio/provincia), sigue por el país y después amplíala al mundo entero. Por último, también puedes recurrir a mecanismos alternativos para obtener fondos, como las campañas de crowdfunding o eventos locales de recaudación de fondos.