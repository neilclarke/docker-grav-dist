---
title: What are the different funding sources needed to implement your project?
taxonomy:
    category: docs
---

So, now you have a project plan and a budget. Next, it’s time to figure out how to get the funds to actually put your project in action. Therefore, it is important to research different potential sources of funding, depending on the aim and topic of your project. Donors can be diverse and include private sector (companies), foundations, and international organizations. Furthermore, perhaps in your community there are specific funding options led by the local and/or national government. Try to be organized in your search, start locally (in your municipality/province), then nationally, and then expand to a worldwide search. Finally, you can also consider alternative ways to raise funds, such as crowdfunding campaigns or local fundraising events.