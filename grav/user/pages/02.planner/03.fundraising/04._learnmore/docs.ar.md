---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية

-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	كيفية جمع تبرعات عن طريق الفيسبوك
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">FIRST Lego League</a>       
	جمع التبرعات بست خطوات (فيلم فيديو)
-	<a target="_blank" href="http://www.youthassembly.nyc/fundraising-guide/">The Youth Assembly at the United Nations</a>   
	استراتيجية لجميع التبرعات على المستوى الشعبي
-	<a target="_blank" href="http://www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">UNICEF</a>     
	بعض الأفكار لأنشطة جمع التبرعات

