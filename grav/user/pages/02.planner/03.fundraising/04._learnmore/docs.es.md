---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías

-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	Cómo recaudar fondos en Facebook
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">FIRST Lego League</a>       
	La recaudación de fondos en seis pasos (vídeos)
-	<a target="_blank" href="http://www.youthassembly.nyc/fundraising-guide/">The Youth Assembly at the United Nations</a>   
	Estrategia para recaudar fondos a nivel popular
-	<a target="_blank" href="http://www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">UNICEF</a>     
	Ideas para actividades de recaudación de fondos