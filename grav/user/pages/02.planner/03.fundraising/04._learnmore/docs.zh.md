---
title: 想了解更多？
displaytitle: 工具和指南链接
taxonomy:
    category: docs
hasinlinefield: 'false'
---
工具和指南链接

-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	如何Facebook上集资
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">FIRST Lego联盟</a>       
	六步融资法（视频）
-	<a target="_blank" href="http://www.youthassembly.nyc/fundraising-guide/">联合国青年大会</a>   
	基层融资战略
-	<a target="_blank" href="http://www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">联合国儿童基金会</a>     
	融资活动构想