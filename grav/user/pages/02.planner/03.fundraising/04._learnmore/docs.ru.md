---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации.

-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	Как собрать средства с помощью Facebook
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">Первая Лига Лего</a>       
	Шесть этапов привлечения средств (видеоматериалы)
-	<a target="_blank" href="http://www.youthassembly.nyc/fundraising-guide/">Молодежная ассамблея Организации Объединенных Наций</a>   
	Стратегия привлечения средств на местном уровне 
-	<a target="_blank" href="http://www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">ЮНИСЕФ</a>     
	Идеи по привлечению средств