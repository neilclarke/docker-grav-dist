---
title: Pour en savoir plus!
menu: Pour en savoir plus!
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives

-  <a target="_blank" href="https://nonprofits.fb.com/raise-funds/">Facebook</a>   
	Comment lever des fonds sur Facebook
-  <a href="https://www.firstinspires.org/resource-library/fundraising-toolkit" target="_blank">FIRST Lego League</a>       
	La collecte de fonds en six étapes (vidéos)
-	<a target="_blank" href="http://www.youthassembly.nyc/fundraising-guide/">Assemblée mondiale de la jeunesse des Nations Unies</a>   
	Stratégie de collecte de fonds au niveau local 
-	<a target="_blank" href="http://www.unicef.ca/sites/default/files/legacy/imce_uploads/images/ce/events/fundraising_ideas_8_dec_2014.pdf">UNICEF</a>     
	Idées pour organiser des activités de collecte de fonds

