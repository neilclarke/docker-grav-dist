---
title: ¿Qué requisitos o condiciones tiene(n) el(los) donante(s)?¿Cumples los criterios?
taxonomy:
    category: docs
---
Para obtener financiación de determinadas instituciones, normalmente tendrás que realizar aplicaciones y en muchos casos competir con otras propuestas de proyecto. Redactar solicitudes de financiación lleva tiempo y requiere dedicación. Antes de hacer una solicitud, lee cuidadosamente las condiciones y los requisitos. Si cumples todos, redacta la solicitud. Si puedes, haz que alguien más lea la aplicación y dé su opinión para que la redacción sea la mejor posible. Cuando elabores un plan de obtención de fondos incluye las diferentes fuentes de financiación, los requisitos, las condiciones y los plazos. También deberás tener en cuenta el tiempo que pasará entre que envíen las solicitudes hasta que se reciban los fondos.