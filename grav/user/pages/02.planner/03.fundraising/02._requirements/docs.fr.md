---
title: Quelles sont les exigences des donateurs ? Est-ce que vous remplissez les critères ?
taxonomy:
    category: docs
---

Lorsque vous sollicitez des institutions, vous devez généralement remplir un formulaire de demande de financement. Le plus souvent, votre projet sera mis en concurrence avec d’autres propositions. La rédaction des demandes de financement exige que l’on y consacre beaucoup de temps et d’efforts. Avant de présenter une demande, lisez avec attention les exigences requises. Si vous répondez à toutes ces exigences, commencez à rédiger votre demande. Si possible, demandez à quelqu’un d’autre de relire votre demande et de vous faire part de ses observations. Votre demande doit être la meilleure possible! Lorsque vous préparez un plan de collecte de fonds, faites-y figurer les différentes sources de financement, les exigences requises et les délais à respecter. Tenez également compte du temps qui s’écoulera entre le moment où vous préparerez votre dossier de demande de financements et celui où vous recevrez les subventions.