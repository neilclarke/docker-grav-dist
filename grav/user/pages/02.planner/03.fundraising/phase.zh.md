---
title: 项目融资
subtitle: 第3阶段
menu: 融资
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category:
        - docs
---
关于为实施项目争取必要资金的问题