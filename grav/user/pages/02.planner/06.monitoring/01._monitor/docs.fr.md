---
title: Comment procéder au suivi de votre projet ? 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Comment savoir si les objectifs du projet ont été atteints ? Une fois que vous aurez décomposé vos objectifs en tâches clés à effectuer, vous devrez déterminer comment mesurer leurs résultats ainsi que les principaux effets produits par votre projet. C’est ce que l’on appelle le suivi, auquel il est conseillé de procéder dès le début du projet. Il s’agit d’une opération cruciale qui permet d’obtenir les éléments nécessaires pour montrer que le projet a réellement accompli ce pour quoi il a été conçu. Il est donc indispensable de mettre en place un mécanisme qui vous permettra de mesurer les résultats; c’est ce que l’on appelle souvent des indicateurs. Il vous faudra définir des indicateurs correspondant aux différentes tâches définies durant la phase de mise en œuvre et de coordination du projet afin de déterminer dans quelle mesure le projet est en bonne voie d’atteindre les objectifs fixés. Gardez à l’esprit que le plan de suivi doit être établi avant de lancer la mise en œuvre du projet. Enfin, une fois que ce plan est prêt et que la mise en œuvre est lancée, n’oubliez pas d’effectuer un suivi à intervalle régulier (tous les 3 ou 4 mois par exemple).