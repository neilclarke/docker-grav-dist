---
title: 'How will you monitor your project?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

How will you know if you have achieved the project’s objectives? Once you’ve broken down your objectives into key activities, you need to define _how_ to track the results of these activities as well as the main impact of your project. This is called monitoring and it should be done from the beginning of the project. It is crucial because it’s a mechanism that will provide the evidence required to show that the project actually accomplished what it was designed to do. Therefore, you need to identify a mechanism for _measuring_ results; these are often called indicators.

You should define indicators for the different activities (which you identified in the _Project Implementation and Coordination_ phase) in order to help you measure how well the project is reaching the established objectives. Keep in mind that the monitoring plan is designed before starting the implementation of your project. Finally, once you have your monitoring plan ready, and actually start implementing your project don’t forget to regularly monitor (a suggestion can be every 3–4 months). 