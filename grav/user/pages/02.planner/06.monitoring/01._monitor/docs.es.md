---
title: ¿Cómo harás el seguimiento de tu proyecto?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
¿Cómo sabrás si has alcanzado los objetivos del proyecto? Después de dividir los objetivos en actividades esenciales, tendrás que definir cómo monitorear los resultados de esas actividades y el impacto principal de tu proyecto. Esto es lo que llamamos seguimiento y se debe hacer desde el inicio del proyecto. Es crucial porque es un mecanismo que proporcionará la evidencia de cómo el proyecto alcanzó aquello para lo que se diseñó. Por consiguiente, tendrás que adoptar mecanismos para medir los resultados; a esos mecanismos se les llama a menudo indicadores. Tendrás que definir indicadores para las diferentes actividades (señaladas en la fase de Ejecución y coordinación del proyecto) que te ayuden a medir hasta qué punto el proyecto está alcanzando los objetivos establecidos. Ten en mente que el plan de seguimiento se diseña antes de empezar a ejecutar el proyecto. Por último, cuando ya tengas listo el plan de seguimiento y comiences a ejecutar el proyecto, no olvides hacer el seguimiento periódicamente (sugerimos hacerlo cada 3 o 4 meses).