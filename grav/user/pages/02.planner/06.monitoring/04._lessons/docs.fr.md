---
title: Avez-vous dégagé 5 enseignements à tirer et formulé 5 recommandations ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Quels sont les enseignements que d’autres pourront tirer de votre expérience ? Comment les autres pourront éviter les difficultés auxquelles vous avez dû faire face ? A la fin du projet, il est indispensable de noter par écrit les difficultés que vous aurez rencontrées tout au long de la mise en œuvre ainsi que les solutions que vous aurez trouvées pour y remédier. Efforcez-vous de décrire ce qui a bien ou moins bien marché, indiquez à l’attention d’autres équipes de projet en quoi vous leur conseilleriez d’agir différemment. Vous pouvez intégrer ces éléments au rapport final du projet. Ces informations seront une aide précieuse pour d’autres organisations de jeunesse qui se lancent dans des initiatives similaires, mais aussi pour les projets futurs de votre propre organisation.