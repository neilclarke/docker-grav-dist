---
title: Have you identified 5 lessons learned and 5 recommendations?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

What can others learn from your experience? How can others avoid the challenges that you faced? Once the project is completed, it is important to document the difficulties you encountered throughout its implementation and what you did to solve them. Try to share what worked well and what didn’t, and what you would recommend other project teams do differently. You can include them in your project’s final report. This information can help other youth organizations trying to undertake similar initiatives and it can also be useful for future projects of your organization.