---
title: ¿Has identificado cinco aprendizajes y cinco recomendaciones? 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
¿Qué enseñanzas pueden obtener otras personas de tu experiencia? ¿Cómo pueden evitar los desafíos que tu enfrentaste? Una vez terminado el proyecto, es importante documentar las dificultades que hayas encontrado a lo largo de su ejecución y qué hiciste para superarlas. Intenta compartir lo que dio buenos resultados y lo que no funcionó y qué recomendarías que se hiciese de otro modo en otros proyectos. Puedes incluirlo en el informe final de tu proyecto. Esta información puede ayudar a otras organizaciones juveniles que intentan llevar a cabo iniciativas similares y también puede ser útil para futuros proyectos de tu organización. 