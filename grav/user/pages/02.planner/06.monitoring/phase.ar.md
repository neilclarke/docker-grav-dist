---
title: رصد المشروع وإعداد تقارير بشأنه وتقييمه
subtitle: المرحلة السادسة
menu: رصد
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
أسئلة بشأن رصد مشروعك وتقييمه
