---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية

-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Evaluation Toolbox</a>   
	إعداد خطة رصد بسبع خطوات سهلة
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	كيف تستخدم جوجل أناليتكس (Google Analytics) في عملية الرصد
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>   
	رصد المشاريع وفقاً لكتاب صادر عن هيئة الأمم المتحدة للمرأة- الصفحات 126-146
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Wikipedia</a>     
	ما هو الرصد والتقييم
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">World Health Organization</a>   
	كيفية إعداد خطة للرصد والتقييم- الصفحات 100-104