---
title: 'Want to learn more?'
taxonomy:
    category:
        - docs
menu: 'Learn More'
slug: _learnmore
hasinlinefield: 'false'
---

-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Evaluation Toolbox</a>   
	Develop a monitoring plan in seven easy steps
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	How to use Google Analytics for monitoring
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>   
	Project monitoring according to UN Women, pp. 126–146
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Wikipedia</a>     
	What is monitoring and evaluation (M&E)? 
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">World Health Organization</a>   
	How to build a monitoring and evaluation plan, p. 100–104