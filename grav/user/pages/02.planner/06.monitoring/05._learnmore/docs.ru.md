---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации.

-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Набор инструментов для проведения оценки</a>   
	Разработка плана проведения мониторинга: семь простых шагов
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	Как использовать инструмент Google Analytics для проведения мониторинга
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ООН-женщины</a>   
	Мониторинг проекта в соответствии с требованиями структуры «ООН-женщины» (с. 126-146)
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Википедия</a>     
	Что такое мониторинг и оценка (МиО)?
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">Всемирная организация здравоохранения</a>   
	Как разработать план мониторинга и оценки (с. 100-104)