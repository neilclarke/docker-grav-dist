---
title: 想了解更多？
menu: 想了解更多？
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">评估工具箱</a>   
	制定监测计划，只需简单7步
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	如何利用谷歌分析网站进行监测
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">联合国妇女署</a>
	《联合国妇女署教你如何进行项目监测》，第126-146页
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">维基百科</a>     
	什么是监测和评估（ME）？
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">世界卫生组织</a>   
	如何制定监测和评估计划，第100-104页