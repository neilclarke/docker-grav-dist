---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías

-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Evaluation Toolbox</a>   
	Cómo elaborar un plan de seguimiento en siete pasos fáciles
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	Cómo utilizar Google Analytics para el seguimiento
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Mujeres</a>   
	El seguimiento de los proyectos según ONU Mujeres - págs. 126-146
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Wikipedia</a>     
	¿Qué es el Seguimiento y Evaluación (SyE)?
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">Organización Mundial de la Salud</a>   
	Cómo elaborar un plan de seguimiento y evaluación -  págs. 100-104