---
title: Pour en savoir plus!
menu: Pour en savoir plus!
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives

-  <a target="_blank" href="http://evaluationtoolbox.net.au/index.php?option=com_content&view=article&id=20&Itemid=159">Evaluation Toolbox</a>   
	Elaborez un plan de suivi en sept étapes
-  <a href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab5" target="_blank">Google</a>       
	Comment utiliser Google Analytics dans le cadre du suivi
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Femmes</a>   
	Le suivi de projet selon ONU Femmes – pp. 126-146
-	<a target="_blank" href="https://en.wikipedia.org/wiki/Monitoring_and_evaluation">Wikipedia</a>     
	Que signifient suivi et évaluation ?
-	<a target="_blank" href="http://www.who.int/tdr/publications/year/2014/9789241506960_workbook_eng.pdf">World Health Organization</a>   
	Comment élaborer un plan de suivi et d’évaluation  – p. 100-104.