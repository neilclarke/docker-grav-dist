---
title: Seguimiento, presentación de informes y evaluación del proyecto
subtitle: Fase 6
menu: Seguimiento
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Preguntas sobre el seguimiento y la evaluación de tu proyecto.