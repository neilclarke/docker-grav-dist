---
title: ¿Cómo vas a documentar y a evaluar los resultados del proyecto? 
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Una vez concluido el proyecto, tú (y tu equipo) deben redactar los resultados del proyecto. Trata de exponer con el mayor detalle posible qué ha alcanzado el proyecto y cómo lo ha hecho. Anteriormente se señaló que tal vez los donantes exijan que se les presenten informes de los proyectos, sin embargo, aunque no lo hagan, deberías redactar un informe. Los informes son instrumentos de comunicación, que son útiles compartir con la comunidad y las autoridades locales/nacionales. Asimismo, son aportes valiosos para futuros proyectos. 

En cuanto a las evaluaciones, normalmente se realizan al final de un proyecto. Su finalidad es determinar si el proyecto generó un impacto o un cambio significativo, cosa que, por lo general, solo puede medirse a largo plazo. Así pues, se recomienda efectuar evaluaciones a proyectos que duren al menos dos años. Si tu proyecto es a largo plazo, considera realizar una evaluación. No olvides que también tiene consecuencias financieras, ya que habrá que contratar a un experto externo para que la dirija, por lo que se deben incluir sus honorarios en el presupuesto.  