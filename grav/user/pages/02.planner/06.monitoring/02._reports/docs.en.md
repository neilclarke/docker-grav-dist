---
title: Do the donors require reports?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Before you start implementing your project take into consideration the information that you will be required to submit to the donor(s) once your project is finalized. Then, determine how you will capture this information throughout the project. Actually, you can also add this to your monitoring plan. Take into consideration that some donors might have mandatory report templates and/or formats that you will be asked to submit upon finalization of the project. Reports for donors can include both a financial summary as well as the results achieved by the project.