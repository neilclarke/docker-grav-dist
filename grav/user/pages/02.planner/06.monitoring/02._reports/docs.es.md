---
title: ¿Los donantes exigen informes?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Antes de empezar a ejecutar tu proyecto, ten en cuenta la información que tendrás que suministrar al/a los donante(s) cuando el proyecto haya concluido. Luego, decide cómo irás capturando esta información a lo largo del proyecto, y que también se puede añadir al plan de seguimiento. Algunos donantes pueden tener plantillas y/o formularios de informes de uso obligatorio que te pedirán que presentes una vez concluido el proyecto. Los informes para los donantes pueden incluir un resumen financiero y los resultados del proyecto.