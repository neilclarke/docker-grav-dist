---
title: Les donateurs demandent-ils des rapports ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Avant de lancer la mise en œuvre de votre projet, faites le point sur les informations que vous devrez présenter au(x) donateur(s) à la fin du projet. Réfléchissez ensuite à la manière dont vous collecterez ces informations pendant toute la durée du projet. Tous ces éléments pourront également être intégrés à votre plan de suivi. Attention, certains donateurs définissent des modèles ou des schémas de présentation des rapports à produire une fois le projet terminé. Les rapports destinés aux donateurs peuvent inclure à la fois un résumé financier et les résultats obtenus à l’issue du projet.