---
title: ¿Qué recursos se necesitan para ejecutar tu proyecto?
taxonomy:
    category: docs
---
Un buen presupuesto es lo más concreto posible y está formado por recursos, cantidades y costos. En primer lugar, es importante determinar qué recursos materiales se necesitan y qué cantidades de cada uno necesitas para alcanzar tus objetivos. Esos recursos se pueden dividir en diferentes categorías, por ejemplo, recursos humanos, equipo técnico, materiales/suministros, comunicaciones/publicaciones. Elabora una lista de lo que necesitarás y en qué cantidad, con la mayor precisión posible.