---
title: 'How much would it cost to implement your project?'
taxonomy:
    category:
        - docs
---

It is key to estimate how much it will cost to implement your project within the timeframe that you have set. After identifying the resources needed and their corresponding quantities you should assign costs to each resource. In order to make accurate cost estimations, research online to have up-to-date information, ask people who are experienced, and try to look at budgets of similar projects. Note that there are two types of costs: direct costs and indirect costs, and both should be included in your budget. Also, keep in mind that it is important to know the estimation units. Are your costs per hour, per event, per person? Try to be consistent.

Furthermore, consider alternatives so that the budget is not only realistic but also cost-effective. For example, if you want to do a workshop, it might be less expensive to do it in a public facility rather than renting out a venue. Remember to review national norms and regulations, for example concerning salaries. Do you need to pay taxes (perhaps value added taxes)? This should also be included in the budget. Once finalized, have another look at your budget, did you identify all costs? You can even consider adding a small margin for miscellaneous costs, these are unexpected expenses that might come up during the implementation of the project. Finally, you should know your budget thoroughly, as it will be important that you can justify the costs of your project to possible donors.