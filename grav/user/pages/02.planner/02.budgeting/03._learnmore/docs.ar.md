---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية

-  <a target="_blank" href="www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">International Budget Partnership</a>   
	الدليل الإرشادي لأعمال الموازنة للمنظمات غير الحكومية (5 لغات)
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Project Management videos</a>   
	نصائح بشأن إدارة تكاليف المشروع (فيلم فيديو)
-	<a target="_blank" href="siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">World Bank</a>     
	نصائح بشأن الميزنة من البنك الدولي

