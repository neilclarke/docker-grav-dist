---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías

-  <a target="_blank" href="http://www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">International Budget Partnership</a>   
	Guía para elaborar presupuestos de ONG (en cinco idiomas)
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">Microsoft Office</a>       
	Plantillas de presupuestos
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Project Management videos</a>   
	Consejos sobre gestión de costos de proyectos (vídeo)
-	<a target="_blank" href="http://siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">Banco Mundial</a>     
	Consejos del Banco Mundial sobre presupuestación
