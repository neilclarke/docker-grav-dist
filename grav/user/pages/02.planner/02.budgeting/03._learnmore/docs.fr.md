---
title: Pour en savoir plus!
menu: Pour en savoir plus!
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives

-  <a target="_blank" href="http://www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">International Budget Partnership</a>   
	Guide sur le travail budgétaire pour les ONG (en 5 langues)
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">Microsoft Office</a>       
	Modèles de budget
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Vidéos sur la gestion de projet</a>   
	Conseils sur la gestion des coûts d’un projet (vidéo)
-	<a target="_blank" href="http://siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">Banque Mondiale</a>     
	Conseils concernant la budgétisation