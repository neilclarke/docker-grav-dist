---
title: 'Want to learn more?'
taxonomy:
    category:
        - docs
menu: 'Learn more'
slug: _learnmore
hasinlinefield: 'false'
---

-  <a target="_blank" href="http://www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">International Budget Partnership</a>   
	A guide for NGO budget work (5 languages)
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">Microsoft Office</a>       
	Budget templates
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Project Management Videos</a>   
	Project cost management tips (video) 
-	<a target="_blank" href="http://siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">World Bank</a>     
	Budgeting tips from World Bank 
