---
title: 想了解更多？
displaytitle: 请戳以下连接
taxonomy:
    category: docs
hasinlinefield: 'false'
---
请戳以下连接

-  <a target="_blank" href="http://www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">国际预算伙伴关系</a>   
	非政府组织预算工作指南（5种语言）
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">微软Office软件</a>       
	预算模板
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">项目管理视频</a>   
	项目成本管理建议（视频）
-	<a target="_blank" href="http://siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">世界银行</a>     
	世界银行预算编制建议
