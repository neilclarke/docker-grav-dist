---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category: docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации.

-  <a target="_blank" href="http://www.internationalbudget.org/publications/a-guide-to-budget-work-for-ngos">Международное бюджетное</a>   
	Руководство для НПО по вопросам подготовки бюджета (на пяти языках)
-  <a href="https://templates.office.com/en-au/Budgets" target="_blank">Microsoft Office</a>       
	типовые шаблоны бюджета
-	<a target="_blank" href="https://www.youtube.com/watch?v=oXhgwn-girI">Видеоматериалы по тематике управления проектом</a>   
	Рекомендации в отношении управления расходами по проекту (видеоролик)
-	<a target="_blank" href="http://siteresources.worldbank.org/INTBELARUS/Resources/Budgeting.pdf">Всемирный банк</a>     
	Рекомендации по составлению бюджета, подготовленные Всемирным банком
