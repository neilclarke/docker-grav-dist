---
title: إعداد ميزانية المشروع
subtitle: المرحلة الثانية
menu: ميزانية
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category:
        - docs
---
أسئلة بشأن ميزانية مشروعك