---
title: Составление сметы проекта
subtitle: Этап 2
menu: сметы проекта
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category:
        - docs
---
Вопросы, касающиеся бюджета вашего проекта:
