---
title: Identificación y planificación del proyecto
subtitle: Fase 1
menu: Identificación
image: phase-1-image.png
jumbo: phase-1-jumbo.png
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category:
        - docs
---
Preguntas sobre tu proyecto en la etapa de identificación y planificación