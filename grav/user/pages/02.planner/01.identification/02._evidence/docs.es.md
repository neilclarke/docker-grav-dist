---
title: ¿Qué evidencia y/o datos tienes que respalden la realidad del problema identificado?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Aparte de necesitar una buena definición del problema también es importante tener evidencia y/o datos que demuestren que el problema existe realmente. Para ello es preciso que investigues sobre el problema que hayas detectado. Las fuentes de datos y evidencias pueden ser estadísticas, resultados de encuestas e información procedente de informes anteriores obra de organizaciones internacionales, organizaciones no gubernamentales (ONG) y/o instituciones oficiales. Las investigaciones basadas en datos y en pruebas aumentan la validez de tu proyecto. Esta información es importante porque ayudará a otras personas a comprender por qué el problema detectado es un tema que vale la pena atender.