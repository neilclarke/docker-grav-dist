---
title: Quels sont les éléments factuels/les données qui confirment que le problème existe ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Il ne suffit pas de bien poser le problème, il est tout aussi important de disposer d’éléments factuels solides et/ou de données pour prouver que le problème existe bel et bien, d’où la nécessité de mener quelques recherches. Les sources de données et d’éléments factuels comprennent notamment les statistiques, les résultats d’enquêtes et les informations provenant de rapports publiés par des organisations internationales, des organisations non gouvernementales (ONG) et/ou des organismes publics. Les données et les éléments que vous aurez tirés de vos recherches donneront une plus grande légitimité à votre projet. Ces informations revêtent une importance cruciale car elles aideront d’autres personnes à comprendre pourquoi le problème que vous soulevez est un défi qui mérite d’être relevé.