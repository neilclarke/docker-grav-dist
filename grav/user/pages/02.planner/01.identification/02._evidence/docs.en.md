---
title: 'What evidence and/or data do you have that supports the identified problem?'
taxonomy:
    category:
        - docs
displaytitle: 'What evidence and/or data do you have that supports the identified problem?'
---

You not only need a good problem definition, but it is also equally important to have solid evidence and/or data to prove that the problem actually exists. This implies doing some research on the problem you have identified. Sources of data and evidence can include statistics, survey results, and information from previous reports elaborated by International Organizations, Non-Governmental Organizations (NGOs) and/or government institutions. Data and evidence-based research add validity to your project. This information is crucial as it will help others understand why the problem identified is a worthy issue to tackle.