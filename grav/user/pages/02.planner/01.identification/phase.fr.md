---
title: Identification et planification du projet
subtitle: Étape 1
menu: Identification
image: phase-1-image.png
jumbo: phase-1-jumbo.png
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category:
        - docs
---
Questions concernant votre projet à l’étape de l’identification et de la planification