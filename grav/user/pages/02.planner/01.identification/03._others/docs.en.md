---
title: 'What other initiatives have been implemented that target the same problem?'
taxonomy:
    category:
        - docs
---

It is likely that other youth-led organizations might have already done similar projects to address the problem you’ve identified. Before settling on your project, investigate what other initiatives have been implemented on the same topic. This will be helpful because you can learn from previous results, and the lessons and challenges from other Projects. You might also get new ideas that can improve your project design. 