---
title: Quelles sont les initiatives qui ont déjà été menées pour remédier à ce problème ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Il n’est pas impossible que d’autres organisations de jeunesse aient déjà mis en œuvre des projets similaires afin d’apporter des solutions au problème qui vous préoccupe. Avant de vous lancer, passez en revue les initiatives qui ont déjà été menées pour traiter cette question. Cette démarche est très utile car les résultats obtenus par les autres projets, de même que les difficultés qu’ils ont rencontrées et les leçons qu’ils ont tirées de leur expérience, seront pour vous une source d’enseignement. Vous pourrez en outre glaner quelques idées qui vous permettront d’améliorer l’élaboration de votre projet.