---
title: 'What are the objectives and/or expected results of your project?'
metadata:
    name: objectives-and-results
    anchor: objectives-and-results
taxonomy:
    category:
        - docs
---

What is the project going to be about? If you had previously identified the problem, it is now time to reflect on what you want to *achieve* and *how*. Keep in mind that you don’t have to solve every dimension of the problem you identified. Be realistic. It is better to have only a few targets (2–4) which can actually be attained.