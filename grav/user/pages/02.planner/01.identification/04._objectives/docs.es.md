---
title: ¿Cuáles son los objetivos y/o los resultados previstos de tu proyecto?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
¿De qué va a tratar el proyecto? Si ya has detectado previamente el problema, es hora de reflexionar acerca de lo que quieres conseguir y cómo. No tienes que resolver todas las dimensiones del problema. Sé realista. Más vale tener solo unas cuantas metas (entre 2 y 4) que realmente puedan cumplirse.