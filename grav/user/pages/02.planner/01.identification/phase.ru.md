---
title: Определение параметров и планирование проекта
subtitle: Этап 1
menu: параметров
image: phase-1-image.png
jumbo: phase-1-jumbo.png
cache_enable: true
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
process:
    markdown: false
taxonomy:
    category:
        - docs
---
Вопросы, касающиеся вашего проекта на этапе определения параметров и планирования