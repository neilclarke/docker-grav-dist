---
title: 'What is the timeframe of your project?'
taxonomy:
    category:
        - docs
---

It is important to determine how much time it will take to reach the established objectives. A project has a clearly defined timeframe, and we must do our best to stick to it. In order to establish this timeframe – that could range from three months to a couple of years – try to consider how many people will be in your team, how fast you can get the funding to kickstart the project, and how long it will take to coordinate with the different stakeholders involved in the project. 