---
title: Quelle est la durée prévue de votre projet ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Il est important d’évaluer le temps qu’il vous faudra pour atteindre les objectifs fixés. Un projet s’inscrit dans un calendrier bien défini, et tout doit être fait pour s’y tenir. Afin de définir ce calendrier – la durée du projet peut aller de trois mois à plusieurs années –, essayez de tenir compte des points suivants : combien de personnes composeront votre équipe, combien de temps vous faudra-t-il pour réunir les financements nécessaires au lancement du projet, quel sera le temps nécessaire pour coordonner vos efforts avec les autres parties prenantes du projet.