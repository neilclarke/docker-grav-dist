---
title: Who will your project help?
taxonomy:
    category:
        - docs
---

It is important to know who your project supports and what value it will bring to them. Are you benefiting a specific group of people, a community, a particular geographic area? Most importantly, how is your project truly addressing their needs? Don’t assume that you know everything about the beneficiaries, talk to them, ask questions, this will help you better define your project objectives, and improve the design of your project. Plus, this is also a way to ensure community engagement, interest and participation. 