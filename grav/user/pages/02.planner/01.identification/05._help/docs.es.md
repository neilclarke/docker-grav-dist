---
title: ¿A quién ayudará tu proyecto?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Es importante saber a quiénes apoya tu proyecto y qué valor les aportará. ¿Va a beneficiar a un grupo específico de personas, a una comunidad, una zona geográfica determinada? Sobre todo, ¿cómo aborda tu proyecto realmente sus necesidades? No des por sentado que tienes un conocimiento exhaustivo de los beneficiarios; habla con ellos, hazles preguntas; esto ayudará a definir mejor los objetivos y a mejorar el diseño de tu proyecto, además de ser una manera de obtener el compromiso, el interés y la participación de la comunidad.