---
title:  你要解决哪些问题
slug: '_problem'
taxonomy:
    category: docs
process:
	twig: true
---
要有一个好的项目，首先要有**准确的问题定位**。无论是国家、社区还是学校里的问题, 第一步必然是确定需要解决的问题，。一般情况下要说明你力求**改变**什么，以及作出改变的**理由**。要想知道你有没有把问题说清楚，就要看你能不能用一句话来概括这个问题。