---
title: 'What is the problem that you want to solve?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

A good project comes from a _good problem definition_. It is important to first identify the problem you want to address, whether it be in your country, community or school. In general, try to describe _what_ you want to change and _why_ you want to change it. A way to test the clarity of your problem statement is to see if you can summarize it in one sentence.