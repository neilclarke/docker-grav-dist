---
title: ¿Qué problema quieres resolver?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Un buen proyecto resulta de una buena definición del problema. Es importante determinar primero el problema que deseas abordar, ya sea en tu país, comunidad o escuela. En general, intenta describir qué quieres cambiar y por qué quieres cambiarlo. Una forma de verificar la claridad de tu definición del problema es ver si puedes resumirlo en una frase.