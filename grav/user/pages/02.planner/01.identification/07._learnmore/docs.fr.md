---
title: Pour en savoir plus!
menu: Pour en savoir plus
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	Kit de proposition de projet (contient des modèles et des échantillons)
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">Project Management videos</a>       
	Comment rédiger une proposition de projet (vidéo)
-	<a target="_blank" href="https://strategyzer.com">Strategyzer</a>   
	Business Model Nouvelle génération 
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Femmes</a>     
	Les conseils d’ONU Femmes pour élaborer un plan de projet précis  - pp. 68-90
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	Comment créer une ONG
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	Comment rédiger un projet d’entreprise