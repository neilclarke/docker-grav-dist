---
title:  想了解更多？
taxonomy:
    category: docs
process:
	twig: true
hasinlinefield: 'false'
---
请戳以下连接

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	项目提案工具包（配有模板和样本）
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">项目管理视频</a>       
	如何撰写项目提案（视频）
-	<a target="_blank" href="https://strategyzer.com">战略决策者</a>   
	商业模式启动手册
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">联合国妇女署</a>     
	《妇女署教你如何制定明晰的项目计划》，第68-90页
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	如何创建一家非政府组织
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	如何撰写商业提案 