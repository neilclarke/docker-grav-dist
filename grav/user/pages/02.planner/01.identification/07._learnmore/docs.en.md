---
title: 'Want to learn more?'
menu: 'Learn More'
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	Project proposal toolkit (with templates & samples)
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">Project Management Videos</a>       
	How to write a project proposal (video)
-	<a target="_blank" href="https://strategyzer.com">Strategyzer</a>   
	Book _Business Model Generation_ 
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>     
	Tips to build a clear project plan from UN Women, pp. 68–90 
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	How to start an NGO 
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	How to write a business proposal 