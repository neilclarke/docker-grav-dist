---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية 

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	مجموعة تعليمية وتدريبية بشأن كيفية اقتراح مشروع ما (نماذج وعينات)
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">Project Management videos</a>       
	كيف تكتب اقتراح مشروع (فيلم فيديو)
-	<a target="_blank" href="https://strategyzer.com">Strategyzer</a>   
	كتاب متاح على الإنترنت عنوانه: Business Model Generation book
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>     
	نصائح بشأن إعداد خطة مشروع واضحة ترد في كتاب صادر عن هيئة الأمم المتحدة للمرأة، الصفحات 68-90
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	كيفية إنشاء منظمة غير حكومية
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	كيفية كتابة عرض تجاري