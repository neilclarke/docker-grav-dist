---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации

-  <a target="_blank" href="https://project-proposal.casual.pm">Casual</a>   
	Набор инструментов для подготовки проектных предложений (с образцами и шаблонами)
-  <a href="https://www.youtube.com/watch?v=jsGBuu88WE0" target="_blank">Видематериалы по тематике управления проектом</a>       
	Как написать проектное предложение? (видеоролик)
-	<a target="_blank" href="https://strategyzer.com">Strategyzer</a>   
	 Книга Business Model Generation («Построение бизнес-моделей»)
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ООН-женщины</a>     
	Рекомендации по разработке четкого плана проекта, подготовленные структурой «ООН-женщины» (с. 68-90) 
-	<a target="_blank" href="http://www.wikihow.com/Start-a-Nonprofit-Organization">WikiHow</a>   
	Как создать НПО
-	<a target="_blank" href="http://www.wikihow.com/Write-a-Business-Proposal">WikiHow</a>         
	Как написать коммерческое предложение