---
title: 'Project Identification and Planning'
taxonomy:
    category:
        - docs
menu: Identification
cache_enable: true
process:
    markdown: false
    twig: false
subtitle: 'Phase 1'
image: phase-1-image.png
jumbo: phase-1-jumbo.png
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
---

Questions about your project at the identification and planning stage