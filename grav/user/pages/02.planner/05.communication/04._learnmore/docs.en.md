---
title: 'Want to learn more?'
taxonomy:
    category:
        - docs
menu: 'Learn More'
slug: _learnmore
hasinlinefield: 'false'
---

-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	How to design a campaign working with the media
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	 How to create a Facebook page for increased visibility
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	How to use Google AdWords for advertising
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>     
	How to use media to promote your project, pp. 120–125 