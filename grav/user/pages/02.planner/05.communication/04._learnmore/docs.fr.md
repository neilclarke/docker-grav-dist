---
title: Pour en savoir plus!
menu: Pour en savoir plus!
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Liens vers des outils et des directives

-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	Comment concevoir une campagne en travaillant avec les médias
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	Comment créer une page Facebook pour accroître sa visibilité 
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	Comment utiliser Google AdWords pour faire de la publicité
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Femmes</a>     
	Comment utiliser les médias pour promouvoir votre projet – pp. 120-125