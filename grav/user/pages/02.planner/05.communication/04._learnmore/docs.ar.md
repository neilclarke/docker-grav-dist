---
title: هل تريد معرفة المزيد؟
menu: هل تريد معرفة المزيد؟
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
وصلات للأدوات والمبادئ التوجيهية 

-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	كيفية تصميم حملة للإعلام والترويج باستخدام وسائل الإعلام
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	كيفية إنشاء صفحة فيسبوك لتسليط المزيد من الضوء على المشروع وأنشطته
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	كيفية استخدام جوجل أدووردز (Google AdWords) للدعاية والإعلان
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">UN Women</a>     
	كيفية استخدام وسائل الإعلام للترويج لمشروعك – الصفحات 120-125