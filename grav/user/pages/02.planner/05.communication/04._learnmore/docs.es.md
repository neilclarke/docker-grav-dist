---
title: ¿Quieres saber más?
menu: ¿Quieres saber más?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Enlaces a instrumentos y guías

-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	Cómo diseñar una campaña trabajando con los medios de comunicación
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	Cómo crear una página en Facebook para aumentar la visibilidad
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	Cómo emplear Google AdWords para anunciarse
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ONU Mujeres</a>     
	Cómo utilizar los medios de comunicación para promover tu proyecto  - págs. 120-125 