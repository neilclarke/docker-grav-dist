---
title: Хотите узнать больше?
menu: Хотите узнать больше?
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
Ссылки на инструменты и методические рекомендации.

-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	Как подготовить информационную кампанию во взаимодействии со СМИ
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	Как создать страницу в Facebook в целях повышения наглядности деятельности
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	Как использовать сервис Google AdWords в рекламных целях
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">ООН-женщины</a>     
	Как использовать СМИ для продвижения вашего проекта (с. 120-125)