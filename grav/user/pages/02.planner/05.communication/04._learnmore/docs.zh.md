---
title: 想了解更多？
menu: 想了解更多？
slug: _learnmore
taxonomy:
    category:
        - docs
hasinlinefield: 'false'
---
-  <a target="_blank" href="http://www.thechangetoolkit.org.au">Change.org</a>   
	如何与媒体合作，共同规划活动
-  <a href="https://nonprofits.fb.com/learn-the-basics/" target="_blank">Facebook</a>       
	如何开设Facebook网页，提高知名度
-	<a target="_blank" href="https://www.google.ca/intl/en/nonprofits/learning/getting-started.html#tab0s">Google for Non profit</a>   
	如何利用谷歌关键字广告，广而告之
-	<a target="_blank" href="https://unwomen.org.au/wp-content/uploads/2015/10/EVAW-Toolkit-UNWomen.pdf">联合国妇女署</a>     
	《教你如何利用媒体宣传项目》，第120-125页