---
title: Comunicaciones y visibilidad del proyecto
subtitle: Fase 5
menu: Comunicaciones
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Preguntas sobre cómo dar a conocer tu proyecto y comunicar acerca de él.