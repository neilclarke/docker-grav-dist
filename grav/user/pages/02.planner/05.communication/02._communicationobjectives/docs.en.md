---
title: 'What are your communications objectives?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Once you have set a target audience, next, it’s time to determine _what_ information will be shared with each type of audience. Your project’s communication objectives will reflect what you want your target audience to learn, take away or act upon. Do you want to persuade, to advocate, or to change a behavior? Do you want to inform your audience about progress made, results achieved? Identify the key messages that you will convey to each type of audience to reach your objectives. Think about the information they need so that you can reach your communication objectives. 