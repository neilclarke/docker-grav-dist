---
title: Quels sont vos objectifs en termes de communication ?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Dès lors que vous aurez défini votre public cible, l’étape suivante consistera à choisir les informations à diffuser auprès de chaque type de public. Les objectifs de votre projet en termes de communication dépendront de ce que vous souhaitez pour votre public cible : ce qu’il apprendra du projet, ce qu’il pourra en retirer, les points sur lesquels il pourra agir. Voulez-vous persuader, faire campagne ou modifier un comportement ? Voulez-vous informer votre public des progrès réalisés, des résultats obtenus ? Formulez les messages clés que vous transmettrez à chaque type de public afin d’atteindre vos objectifs. Décidez des points sur lesquels ils ont besoin d’être informés pour que vous puissiez atteindre les objectifs que vous vous êtes fixés en matière de communication.