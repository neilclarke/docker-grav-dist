---
title: ¿Cuáles son tus objetivos en materia de comunicaciones?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Una vez establecido el público destinatario, lo siguiente será decidir qué información se compartirá con cada tipo de público. Los objetivos en materia de comunicación de tu proyecto reflejarán como quieres que tu audiencia meta actúe o lo que quieres que tu audiencia meta sepa o aprenda. ¿Quieres persuadir, impulsar o modificar un comportamiento? ¿Quieres informar sobre los progresos realizados, los resultados alcanzados? Decide qué mensajes esenciales transmitirás a cada tipo de público para alcanzar tus objetivos. Piensa en la información que necesitas para poder alcanzar tus objetivos en materia de comunicación.