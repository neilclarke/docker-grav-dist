---
title: Communication et visibilité
subtitle: Étape 5
menu: Communication
content:
    items: '@self.modular'
    order:
        by: folder
        dir: asc
taxonomy:
    category: docs
---
Questions pour communiquer et donner de la visibilité à votre projet.