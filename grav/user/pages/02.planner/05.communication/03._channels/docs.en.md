---
title: 'What communications channels will you use?'
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

Once you have your target audience and objectives, you have to identify the channels (or _how_) you will best convey the key messages and reach your target audience. Options can include: social media, websites, print, press. Keep in mind that channels will differ depending on the type of audience. Try to be creative while showing the value of your project in order to stand out. Likewise, you should determine the timing and frequency of your messages (the _when_). Furthermore, consider your constraints, which can be your budget, or access to specific technology. Remember that the most effective ways of reaching out to people is not always the most expensive one. Finally, communication is a job that takes lots of time, keep in mind that a team member might need to take on these tasks full-time.