---
title: ¿Qué canales de comunicación utilizarás?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
Una vez que tengas un público destinatario y hayas determinado tus objetivos de comunicación, deberás escoger los mejores canales (el cómo) para transmitir los mensajes clave y llegar a tu público meta. Algunas opciones son: redes sociales, sitios web, la prensa. Ten presente que los canales diferirán en función del tipo de público. Intenta ser creativo cuando muestres el valor de tu proyecto para así destacar entre otros proyectos. Asimismo, deberás establecer el momento y la frecuencia de tus mensajes (el cuándo). No olvides tus limitaciones, que pueden ser tu presupuesto, o el acceso a un tipo de tecnología. Recuerda que la manera más eficaz de llegar a la gente no siempre es la más cara. Por último, la comunicación es un trabajo que lleva mucho tiempo, podría ser necesario que haya una persona que se dedique a estas tareas a tiempo completo.