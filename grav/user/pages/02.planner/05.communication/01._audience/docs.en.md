---
title: Who is your target audience and why are they important?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---

So, why communications? Sharing and disseminating what you are doing is key in order to keep all those involved in the project informed, but also to generate visibility amongst different stakeholders regarding the work that you are doing. You want people to find out about your project! At the same time, you want people to understand why your project is important, what changes it is generating, and to even encourage them to get involved. In order to share information, you must first identify your target audience. Different types of audience can include: donors, local or national authorities, civil society/ community, other youth organizations. You also have to determine why they are significant, meaning why is it important that they be informed about your project.