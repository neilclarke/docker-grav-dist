---
title: ¿Cuál es tu público objetivo y por qué es importante?
taxonomy:
    category:
        - docs
process:
    markdown: true
    twig: true
---
¿Para qué sirven las comunicaciones? Compartir y difundir lo que estás haciendo es esencial no solo para mantener informadas a todas las personas que intervienen en el proyecto, pero también para generar visibilidad entre los diferentes interesados acerca de la labor que estás realizando. Evidentemente, quieres que la gente conozca tu proyecto y, al mismo tiempo, deseas que entienda por qué es importante, qué cambios está generando, e incluso alentarla a participar. Para compartir información, primero tienes que establecer cuál es tu público objetivo. Los diferentes tipos de público pueden ser: los donantes, las autoridades locales o nacionales, la sociedad civil/la comunidad, otras organizaciones juveniles, etc. También tienes que especificar por qué son importantes, es decir, por qué es importante que estén informados acerca de tu proyecto. 