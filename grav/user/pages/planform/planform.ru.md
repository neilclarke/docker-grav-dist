---
title: My Project Plan
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: Определение параметров и планирование проекта

        - id: form_problem
          name: problem
          label: Какую проблему вы хотите решить?
          title: Какую проблему вы хотите решить?
          placeholder: Определить цель вашего проекта
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: Какими доказательствами и/или данными вы располагаете в подтверждение наличия выявленной проблемы?
          title: Какими доказательствами и/или данными вы располагаете в подтверждение наличия выявленной проблемы?
          placeholder: Добавить дополнительную документацию подтверждающую ваш проект
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: Какие другие инициативы были реализованы для решения этой проблемы?
          title: Какие другие инициативы были реализованы для решения этой проблемы?
          placeholder: Перечислите другие инициативы здесь
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: Каковы цели и/или ожидаемые результаты вашего проекта?
          title: Каковы цели и/или ожидаемые результаты вашего проекта?
          placeholder: Перечислите цели вашего проекта
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: Кому может помочь ваш проект?
          title: Кому может помочь ваш проект?
          placeholder: Укажите кому ваш проект окажет помощь
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: Каковы сроки реализации вашего проекта?
          title: Каковы сроки реализации вашего проекта?
          placeholder: Введите календарный план проекта
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: Составление сметы проекта

        - id: form_resources
          name: resources
          label: Какие ресурсы требуются для осуществления вашего проекта?
          title: Какие ресурсы требуются для осуществления вашего проекта?
          placeholder: Укажите ваши ресурсы проекта
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: Какова стоимость осуществления вашего проекта?
          title: Какова стоимость осуществления вашего проекта?
          placeholder: Введите расчетную стоимость выполнения проекта
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: Привлечение средств на осуществление проекта

        - id: form_sources
          name: sources
          label: Каковы различные источники финансирования, необходимые для реализации вашего проекта?
          title: Каковы различные источники финансирования, необходимые для реализации вашего проекта?
          placeholder: Список потенциальных доноров проекта
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: Чего хочет донор (доноры)? Соответствуете ли вы их критериям?
          title: Чего хочет донор (доноры)? Соответствуете ли вы их критериям?
          placeholder: Введите требования вашего донора и срок подачи заявки
          type: textarea
          classes: ismarkdown

        - id: form_management
          name: management
          label: Каким образом вы планируете использовать полученные средства?
          title: Каким образом вы планируете использовать полученные средства?
          placeholder: Определите, как вы будете управлять проектными средствами
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: Осуществление и координация проекта

        - id: form_activities
          name: activities
          label: Определили ли вы перечень мероприятий, необходимых для достижения целей вашего проекта?
          title: Определили ли вы перечень мероприятий, необходимых для достижения целей вашего проекта?
          placeholder: Введите 2–5 ключевых действия для каждой из ваших целей
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: Могут ли упомянутые мероприятия быть осуществлены с соблюдением установленных сроков и в рамках бюджетной сметы проекта?
          title: Могут ли упомянутые мероприятия быть осуществлены с соблюдением установленных сроков и в рамках бюджетной сметы проекта?
          placeholder: Определите ваш рабочий график
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: Кто войдет в состав вашей команды и чем эти люди будут заниматься?
          title: Кто войдет в состав вашей команды и чем эти люди будут заниматься?
          placeholder: Укажите роли и обязанности всех членов вашей команты
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: Какие другие заинтересованные стороны участвуют в проекте?
          title: Какие другие заинтересованные стороны участвуют в проекте?
          placeholder: Укажите список заинтересованных сторон вашего проекта
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: Что может пойти не так в вашем проекте, и как вы с этим справитесь?
          title: Что может пойти не так в вашем проекте, и как вы с этим справитесь?
          placeholder: Укажите потенциальные риски и механизмы контроля/ответы представить здесь
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: Информационная поддержка и обеспечение наглядности проекта

        - id: form_audience
          name: communications
          label: Кто является вашей целевой аудиторией, и почему эти люди важны для вас?
          title: Кто является вашей целевой аудиторией, и почему эти люди важны для вас?
          placeholder: Опишите целевую аудиторию вашего проекта
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: Какую цель преследует организация вами информационной поддержки?
          title: Какую цель преследует организация вами информационной поддержки?
          placeholder: Опишите ваш план информационной работы проекта
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: Какие каналы связи вы планируете использовать?
          title: Какие каналы связи вы планируете использовать?
          placeholder: Введите здесь каналы связи проекта
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: Мониторинг, отчетность и оценка проекта

        - id: form_monitor
          name: monitor
          label: Как вы намерены осуществлять мониторинг вашего проекта?
          title: Как вы намерены осуществлять мониторинг вашего проекта?
          placeholder: Опишите ваш план мониторинга о выполнении проекта
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: Требуют ли доноры представления отчетности?
          title: Требуют ли доноры представления отчетности?
          placeholder: Опишите ваш план по составлению очетов о выполнении проекта
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: Каким образом вы планируете документировать и оценивать результаты проекта?
          title: Каким образом вы планируете документировать и оценивать результаты проекта?
          placeholder: Опишите как вы будете оценивать свой проект
          type: textarea
          classes: ismarkdown

        - id: form_lessons
          name: lessons
          label: Можете ли вы сформулировать пять выводов и пять рекомендаций на основе усвоенного опыта?
          title: Можете ли вы сформулировать пять выводов и пять рекомендаций на основе усвоенного опыта?
          placeholder: Можете ли вы сформулировать пять выводов и пять рекомендаций на основе усвоенного опыта?
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true


---
# My Project Plan
