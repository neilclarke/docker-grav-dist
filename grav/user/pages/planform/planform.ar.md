---
title: My Project Plan
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: تحديد المشروع وتخطيطه

        - id: form_problem
          name: problem
          label: أسئلة بشأن المشروع في مرحلة تحديده وتخطيطه ما هي المشكلة التي تريد حلها؟
          title: أسئلة بشأن المشروع في مرحلة تحديده وتخطيطه ما هي المشكلة التي تريد حلها؟
          placeholder: حدد هدف المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: ماهي القرائن و/أو البيانات المتوفرة لديك التي تدل على وجود المشكلة المحددة؟
          title: ماهي القرائن و/أو البيانات المتوفرة لديك التي تدل على وجود المشكلة المحددة؟
          placeholder: أضف أدلة داعمة لمشروعك هنا
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: ما هي المشاريع الأخرى التي استهدفت نفس المشكلة والتي جرى تنفيذها؟
          title: ما هي المشاريع الأخرى التي استهدفت نفس المشكلة والتي جرى تنفيذها؟
          placeholder: أدرج مبادرات أخرى هنا
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: ماهي أهداف مشروعك و/أو نتائجه المتوقعة؟
          title: ماهي أهداف مشروعك و/أو نتائجه المتوقعة؟
          placeholder: أدرج أهداف مشروعك هنا
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: من الذين سيساعدهم مشروعك؟
          title: من الذين سيساعدهم مشروعك؟
          placeholder: أدرج من المستفيد من مشروعك هنا
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: ما هو الإطار الزمني لمشروعك؟
          title: ما هو الإطار الزمني لمشروعك؟
          placeholder: أدخل الإطار الزمني للمشروع هنا
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: إعداد ميزانية المشروع

        - id: form_resources
          name: resources
          label: ماهي الموارد المطلوبة لتنفيذ مشروعك؟
          title: ماهي الموارد المطلوبة لتنفيذ مشروعك؟
          placeholder: أدرج موارد المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: كم سيكلف تنفيذ مشروعك؟
          title: كم سيكلف تنفيذ مشروعك؟
          placeholder: أدخل تكلفة المشروع المقدرة هنا
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: جمع التبرعات للمشروع

        - id: form_sources
          name: sources
          label: ما هي مصادر التمويل اللازم لتنفيذ مشروعك؟
          title: ما هي مصادر التمويل اللازم لتنفيذ مشروعك؟
          placeholder: قم بإدراج المانحين المحتملين للمشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: ماهي متطلبات الجهة او الجهات المانحة؟ وفي حلة وجود هذه المتطلبات، فهل يستوفي طلبك او مشروعك المعايير اللازمة؟
          title: ماهي متطلبات الجهة او الجهات المانحة؟ وفي حلة وجود هذه المتطلبات، فهل يستوفي طلبك او مشروعك المعايير اللازمة؟
          placeholder: أدخل متطلبات الجهات المانحة والمواعيد النهائية هنا
          type: textarea
          classes: ismarkdown

        - id: form_management
          name: management
          label: كيف ستدير الأموال؟
          title: كيف ستدير الأموال؟
          placeholder: حدد كيفية إدارة أموال المشروع هنا
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: تنفيذ المشروع وتنسيقه

        - id: form_activities
          name: activities
          label: هل حددت الأنشطة اللازمة للوصول إلى أهداف مشروعك؟
          title: هل حددت الأنشطة اللازمة للوصول إلى أهداف مشروعك؟
          placeholder: دخل 2-5 أعمال رئيسية لكل هدف من أهدافك هنا
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: هل يمكن القيام بهذه الأنشطة ضمن الإطار الزمني للمشروع وميزانيته؟
          title: هل يمكن القيام بهذه الأنشطة ضمن الإطار الزمني للمشروع وميزانيته؟
          placeholder: حدد جدول النشاط الخاص بك هنا
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: من الذي سيكون في فريقك وما الذي سيقومون بعمله؟
          title: من الذي سيكون في فريقك وما الذي سيقومون بعمله؟
          placeholder: أدرج أدوار ومسؤوليات أعضاء فريقك هنا
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: من هي الأطراف الأخرى المعنية التي تشارك في المشروع؟
          title: من هي الأطراف الأخرى المعنية التي تشارك في المشروع؟
          placeholder: أدرج أصحاب المصلحة في مشروعك هناا
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: ما هو وجه الخلل الذي قد يطرأ على مشروعك وكيف ستعالجه إن حدث؟
          title: ما هو وجه الخلل الذي قد يطرأ على مشروعك وكيف ستعالجه إن حدث؟
          placeholder: أدرج المخاطر المحتملة وآليات الرقابة/ردود أفعال هنا
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: الاتصالات وتسليط الضوء على المشروع

        - id: form_audience
          name: communications
          label: ما هو جمهورك المستهدف وما أهميته؟
          title: ما هو جمهورك المستهدف وما أهميته؟
          placeholder: صف جمهورك المستهدف من المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: ماهي أهدافك على صعيد الاتصالات؟
          title: ماهي أهدافك على صعيد الاتصالات؟
          placeholder: صف خطة اتصال المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: ماهي قنوات الاتصال التي ستستخدمها؟
          title: ماهي قنوات الاتصال التي ستستخدمها؟
          placeholder: أدخل قنوات اتصال المشروع هنا
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: رصد المشروع وإعداد تقارير بشأنه وتقييمه

        - id: form_monitor
          name: monitor
          label: كيف سترصد/تراقب سير مشروعك؟
          title: كيف سترصد/تراقب سير مشروعك؟
          placeholder: صف خطة رصد المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: هل تحتاج الجهات المانحة إلى تقارير؟
          title: هل تحتاج الجهات المانحة إلى تقارير؟
          placeholder: صف خطة الإبلاغ عن المشروع هنا
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: كيف ستوثق وتقيّم نتائج المشروع؟
          title: كيف ستوثق وتقيّم نتائج المشروع؟
          placeholder: تحديد كيفية تقييم المشروع الخاص بك هنا
          type: textarea
          classes: ismarkdown

        - id: form_lessons
          name: lessons
          label: هل استخلصت 5 دروس و5 توصيات من تجربتك؟
          title: هل استخلصت 5 دروس و5 توصيات من تجربتك؟
          placeholder: أدرج 5 دروس مستفادة و5 توصيات
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true


---
# My Project Plan
