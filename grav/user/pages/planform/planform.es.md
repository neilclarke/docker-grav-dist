---
title: My Project Plan
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: Identificación y planificación del proyecto

        - id: form_problem
          name: problem
          label: ¿Qué problema quieres resolver?
          title: ¿Qué problema quieres resolver?
          placeholder: Define el objetivo de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: ¿Qué evidencia y/o datos tienes que respalden la realidad del problema identificado?
          title: ¿Qué evidencia y/o datos tienes que respalden la realidad del problema identificado?
          placeholder: Añade datos de apoyo a tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: ¿Qué otras iniciativas se han llevado a cabo que apunten al mismo problema?
          title: ¿Qué otras iniciativas se han llevado a cabo que apunten al mismo problema?
          placeholder: Enumera otras iniciativas aquí
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: ¿Cuáles son los objetivos y/o los resultados previstos de tu proyecto?
          title: ¿Cuáles son los objetivos y/o los resultados previstos de tu proyecto?
          placeholder: Enumera los objetivos de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: ¿A quién ayudará tu proyecto?
          title: ¿A quién ayudará tu proyecto?
          placeholder: Enumera a quiénes ayudará tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: ¿Qué duración tendrá tu proyecto?
          title: ¿Qué duración tendrá tu proyecto?
          placeholder: Ingresa el marco temporal de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: Presupuestación del proyecto

        - id: form_resources
          name: resources
          label: ¿Qué recursos se necesitan para ejecutar tu proyecto?
          title: ¿Qué recursos se necesitan para ejecutar tu proyecto?
          placeholder: Enumera los recursos de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: ¿Cuánto costará ejecutar tu proyecto?
          title: ¿Cuánto costará ejecutar tu proyecto?
          placeholder: Ingresa el coste estimado de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: Recaudación de fondos para el proyecto

        - id: form_sources
          name: sources
          label: ¿Cuáles son las diferentes fuentes de financiación necesarias para ejecutar tu proyecto?
          title: ¿Cuáles son las diferentes fuentes de financiación necesarias para ejecutar tu proyecto?
          placeholder: Enumera los posibles donantes para tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: ¿Qué requisitos o condiciones tiene(n) el(los) donante(s)?¿Cumples los criterios?
          title: ¿Qué requisitos o condiciones tiene(n) el(los) donante(s)?¿Cumples los criterios?
          placeholder: Introduce los requerimientos y fechas límite aquí
          classes: ismarkdown

        - id: form_management
          name: management
          label: ¿Cómo vas a administrar los fondos?
          title: ¿Cómo vas a administrar los fondos?
          placeholder: Define cómo gestionarás los fondos de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: Ejecución y coordinación del proyecto

        - id: form_activities
          name: activities
          label: ¿Has definido las actividades necesarias para alcanzar los objetivos de tu proyecto?
          title: ¿Has definido las actividades necesarias para alcanzar los objetivos de tu proyecto?
          placeholder: Ingresa 2-5 acciones esenciales para cada uno de tus objetivos aquí
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: ¿Es posible realizar las actividades en el tiempo y con el presupuesto establecido?
          title: ¿Es posible realizar las actividades en el tiempo y con el presupuesto establecido?
          placeholder: Define el calendario de tus actividades aquí
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: ¿Quién formará parte de tu equipo y qué hará cada quién?
          title: ¿Quién formará parte de tu equipo y qué hará cada quién?
          placeholder: Enumera los roles y responsabilidades de los miembros de tu equipo aquí
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: ¿Qué otros interesados participan?
          title: ¿Qué otros interesados participan?
          placeholder: Enumera a los interesados de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: ¿Qué podría salir mal y cómo lo afrontarías?
          title: ¿Qué podría salir mal y cómo lo afrontarías?
          placeholder: Enumera los riesgos potenciales y mecanismos de control/respuesta aquí
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: Comunicaciones y visibilidad del proyecto

        - id: form_audience
          name: communications
          label: ¿Cuál es tu público objetivo y por qué es importante?
          title: ¿Cuál es tu público objetivo y por qué es importante?
          placeholder: Describe el público al que se dirige tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: ¿Cuáles son tus objetivos en materia de comunicaciones?
          title: ¿Cuáles son tus objetivos en materia de comunicaciones?
          placeholder: Describe el plan de comunicaciones de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: ¿Qué canales de comunicación utilizarás?
          title: ¿Qué canales de comunicación utilizarás?
          placeholder: Introduce los canales de comunicación de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: Seguimiento, presentación de informes y evaluación del proyecto

        - id: form_monitor
          name: monitor
          label: ¿Cómo harás el seguimiento de tu proyecto?
          title: ¿Cómo harás el seguimiento de tu proyecto?
          placeholder: Describe el plan de seguimiento de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: ¿Los donantes exigen informes?
          title: ¿Los donantes exigen informes?
          placeholder: Describe el plan de informe de tu proyecto aquí
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: ¿Cómo vas a documentar y a evaluar los resultados del proyecto?
          title: ¿Cómo vas a documentar y a evaluar los resultados del proyecto?
          placeholder: Describe cómo vas a evaluar tu proyecto
          type: textarea
          classes: ismarkdown

        - id: form_lessons
          name: lessons
          label: ¿Has identificado cinco aprendizajes y cinco recomendaciones?
          title: ¿Has identificado cinco aprendizajes y cinco recomendaciones?
          placeholder: Incluye cinco aprendizajes y cinco recomendaciones
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true


---
# My Project Plan
