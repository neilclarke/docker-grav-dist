---
title: 项目提
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: 项目设计和规划

        - id: form_problem
          name: problem
          label: 你要解决哪些问题？
          title: 你要解决哪些问题？
          placeholder: 项目目标
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: 既然提出了这个问题，你有哪些证据和/或数据可以作为依据？
          title: O既然提出了这个问题，你有哪些证据和/或数据可以作为依据？
          placeholder: 项目可行性证明
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: 针对这个问题，曾经开展过哪些举措？
          title: 针对这个问题，曾经开展过哪些举措？
          placeholder: 其他项目倡议
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: 你的项目设有哪些目标/预期成果？
          title: 你的项目设有哪些目标/预期成果？
          placeholder: 项目目标
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: 你的项目将帮助到哪些人？
          title: 你的项目将帮助到哪些人？
          placeholder: 项目受益人
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: 如何确定项目时限？
          title: 如何确定项目时限？
          placeholder: 项目时间表
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: 项目预算

        - id: form_resources
          name: resources
          label: 需要哪些资源？
          title: 需要哪些资源？
          placeholder: 项目资源
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: 需要多少资金？
          title: 需要多少资金？
          placeholder: 项目资源
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: 项目融资

        - id: form_sources
          name: sources
          label: 有哪些资金来源？
          title: 有哪些资金来源？
          placeholder: 项目预算
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: 捐助方提出了哪些要求，你如何满足这些条件？
          title: 捐助方提出了哪些要求，你如何满足这些条件？
          placeholder: 项目潜在资助者
          type: textarea
          classes: ismarkdown

        - id: form_management
          name: management
          label: 你如何管理资金？
          title: 你如何管理资金？
          placeholder: 在此处输入您的项目资金管理计划
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: 项目实施和协调

        - id: form_activities
          name: activities
          label: 你是否为实现项目目标确定了必要的活动？
          title: 你是否为实现项目目标确定了必要的活动？
          placeholder: 在此处输入2-5个对应您计划每个目标的措施
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: 能否在项目时限和预算框架内开展这些活动？
          title: 能否在项目时限和预算框架内开展这些活动？
          placeholder: 在此处输入您的活动计划时间表
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: 你的团队有哪些成员？他们将开展哪些工作?
          title: 你的团队有哪些成员？他们将开展哪些工作?
          placeholder: 在此输入您的团队成员的分工与责任
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: W还有哪些利益攸关方会参与进来？
          title: 还有哪些利益攸关方会参与进来？
          placeholder: 在此处输入您的计划相关利益方
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: 你的项目可能会出现哪些问题？你如何解决这些问题？
          title: 你的项目可能会出现哪些问题？你如何解决这些问题？
          placeholder: 在此处输入您的计划的潜在风险与风险控制机制
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: 项目交流和宣传

        - id: form_audience
          name: communications
          label: 你的目标受众是谁？为什么是他们？
          title: 你的目标受众是谁？为什么是他们？
          placeholder: 在此处输入您的计划的目标群体
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: 你为宣传工作设定了哪些目标？
          title: 你为宣传工作设定了哪些目标？
          placeholder: 在此处输入您的宣传计划
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: 你将利用哪些交流渠道？
          title: 你将利用哪些交流渠道？
          placeholder: 在此处输入您的计划的宣传途径
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: 项目监测、报告和评估

        - id: form_monitor
          name: monitor
          label: 如何监测项目？
          title: 如何监测项目？
          placeholder: 在此处输入您的项目运营监管计划
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: 捐助方是否要求提供报告？
          title: 捐助方是否要求提供报告？
          placeholder: 在此处输入您的项目进展报告计划
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: 如何记录和评估项目成果？
          title: 如何记录和评估项目成果？
          placeholder: 在此处输入您的项目评估机制
          type: textarea
          classes: ismarkdown
          
        - id: form_lessons
          name: lessons
          label: 能否总结出5条经验教训，并提出5项建议？
          title: 能否总结出5条经验教训，并提出5项建议？
          placeholder: 包括5个案例和5条建议
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true

---
# 项目提
