---
title: My Project Plan
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: Identification et planification du projet

        - id: form_problem
          name: problem
          label: Quel est le problème auquel vous voulez remédier ?
          title: Quel est le problème auquel vous voulez remédier ?
          placeholder: Définis l'objectif de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: Quels sont les éléments factuels/les données qui confirment que le problème existe ?
          title: Quels sont les éléments factuels/les données qui confirment que le problème existe ?
          placeholder: Ajoute des preuves à l'appui de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: Quelles sont les initiatives qui ont déjà été menées pour remédier à ce problème ?
          title: Quelles sont les initiatives qui ont déjà été menées pour remédier à ce problème ?
          placeholder: Etablis une liste d'autres initiatives ici
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: Quels sont les objectifs et/ou les résultats attendus de votre projet ?
          title: Quels sont les objectifs et/ou les résultats attendus de votre projet ?
          placeholder: Liste les objectifs de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: Qui seront les bénéficiaires de votre projet ?
          title: Qui seront les bénéficiaires de votre projet ?
          placeholder: Etablis une liste des bénéficiaires de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: Quelle est la durée prévue de votre projet ?
          title: Quelle est la durée prévue de votre projet ?
          placeholder: Ajoute la période de ton projet ici
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: Budgétisation

        - id: form_resources
          name: resources
          label: Quelles sont les ressources nécessaires à la réalisation de votre projet ?
          title: Quelles sont les ressources nécessaires à la réalisation de votre projet ?
          placeholder: Liste les ressources de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: Quel est le coût de la réalisation de votre projet ?
          title: Quel est le coût de la réalisation de votre projet ?
          placeholder: Ajoute le coût estimatif de ton projet ici
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: Collecte de fonds

        - id: form_sources
          name: sources
          label: Quelles sont les différentes sources de financements nécessaires à la réalisation de votre projet ?
          title: Quelles sont les différentes sources de financements nécessaires à la réalisation de votre projet ?
          placeholder: Liste les donateurs potentiels de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: Quelles sont les exigences des donateurs ? Est-ce que vous remplissez les critères ?
          title: Quelles sont les exigences des donateurs ? Est-ce que vous remplissez les critères ?
          placeholder: Ajoute les critères pour les donateurs et les délais ici
          type: textarea
          classes: ismarkdown

        - id: form_management
          name: management
          label: Comment gérer les fonds ?
          title: Comment gérer les fonds ?
          placeholder: Définis comment tu géreras les fonds de ton projet ici
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: Mise en œuvre et coordination

        - id: form_activities
          name: activities
          label: Avez-vous défini les activités qui vous permettront d’atteindre les objectifs de votre projet ?
          title: Avez-vous défini les activités qui vous permettront d’atteindre les objectifs de votre projet ?
          placeholder: Ajoute 2 à 5 actions clef pour chacun de tes objectifs ici
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: Ces activités peuvent-elles être réalisées selon les délais fixés et dans les limites du budget ?
          title: Ces activités peuvent-elles être réalisées selon les délais fixés et dans les limites du budget ?
          placeholder: Définis le calendrier de ton activité ici
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: Qui seront les membres de votre équipe, que feront-ils ?
          title: Qui seront les membres de votre équipe, que feront-ils ?
          placeholder: Etablis une liste avec les rôles et les responsabilités de chaque membre de ton équipe ici
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: Quelles sont les autres parties prenantes impliquées ?
          title: Quelles sont les autres parties prenantes impliquées ?
          placeholder: Enumère les parties prenantes de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: Quelles sont les difficultés susceptibles de surgir au cours du projet, comment y faire face ?
          title: Quelles sont les difficultés susceptibles de surgir au cours du projet, comment y faire face ?
          placeholder: Enumère les risques potentiels et les mécanismes de contrôle et de réponse ici
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: Communication et visibilité

        - id: form_audience
          name: communications
          label: Qui sont les personnes cibles, pourquoi sont-elles importantes ?
          title: Qui sont les personnes cibles, pourquoi sont-elles importantes ?
          placeholder: Décris le groupe cible de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: Quels sont vos objectifs en termes de communication ?
          title: Quels sont vos objectifs en termes de communication ?
          placeholder: Décris le plan de communication de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: Quels moyens de communication utiliserez-vous ?
          title: Quels moyens de communication utiliserez-vous ?
          placeholder: Ajoute les chaînes de communication de ton projet ici
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: Suivi et évaluation

        - id: form_monitor
          name: monitor
          label: Comment procéder au suivi de votre projet ?
          title: Comment procéder au suivi de votre projet ?
          placeholder: Décris le plan de suivi de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: Les donateurs demandent-ils des rapports ?
          title: Les donateurs demandent-ils des rapports ?
          placeholder: Décris le plan de rapport de ton projet ici
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: Comment consigner et évaluer les résultats du projet ?
          title: Comment consigner et évaluer les résultats du projet ?
          placeholder: Décris comment tu évalueras ton projet
          type: textarea
          classes: ismarkdown

        - id: form_lessons
          name: lessons
          label: Avez-vous dégagé 5 enseignements à tirer et formulé 5 recommandations ?
          title: Avez-vous dégagé 5 enseignements à tirer et formulé 5 recommandations ?
          placeholder: Incluez 5 leçons tirés et 5 recommandations
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true


---
# My Project Plan
