---
title: My Project Plan
template: planform
form:
    name: plan-form
    fields:

        - type: spacer
          id: identification
          text: Project Identification & Planning

        - id: form_problem
          name: problem
          label: What is the problem that you want to solve?
          title: Problem Definition
          placeholder: Define the goal of your project here
          type: textarea
          classes: ismarkdown

        - id: form_evidence
          name: evidence
          label: What evidence and/or data do you have that supports the identified problem?
          title: Other Initiatives
          placeholder: Add supporting evidence for your project here
          type: textarea
          classes: ismarkdown

        - id: form_others
          name: others
          label: What other initiatives have been implemented that target the same problem?
          title: Evidence
          placeholder: List other intiatives here
          type: textarea
          classes: ismarkdown

        - id: form_objectives
          name: objectives
          label: What are the objectives and/or expected results of your project?
          title: Objectives
          placeholder: List the objectives of your project here
          type: textarea
          classes: ismarkdown

        - id: form_help
          name: help
          label: Who will your project help?
          title: Help
          placeholder: List who your project will help here
          type: textarea
          classes: ismarkdown

        - id: form_timeframe
          name: timeframe
          label: What is the timeframe of your project?
          title: Timeframe
          placeholder: Enter your project timeframe here
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: budgeting
          text: Project Budgeting

        - id: form_resources
          name: resources
          label: What resources are required to implement your project?
          title: Budget
          placeholder: List your project resources here
          type: textarea
          classes: ismarkdown

        - id: form_cost
          name: cost
          label: How much would it cost to implement your project?
          title: Cost
          placeholder: Enter your estimated project cost here
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: fundraising
          text: Project Fundraising

        - id: form_sources
          name: sources
          label: What are the different funding sources needed to implement your project?
          title: Fundraising
          placeholder: List your potential project donors here
          type: textarea
          classes: ismarkdown

        - id: form_requirements
          name: requirements
          label: What are the donor(s) requirements? If so, do you fulfill the criteria?
          title: Requirements
          placeholder: Enter your donor requirements and deadlines here
          type: textarea
          classes: ismarkdown

        - id: form_management
          name: management
          label: How will you manage the funds?
          title: Management
          placeholder: Define how you will manage your project funds here
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: implementation
          text: Project Implementation and Coordination

        - id: form_activities
          name: activities
          label: Have you defined the activities required to reach the objectives of your project?
          title: Activities
          placeholder: Enter 2 to 5 key actions for each of your objectives here
          type: textarea
          classes: ismarkdown

        - id: form_activityschedule
          name: activityschedule
          label: Can these activities be carried out within the project’s timeframe and budget?
          title: Schedule
          placeholder: Define your activity schedule here
          type: textarea
          classes: ismarkdown

        - id: form_team
          name: team
          label: Who will be in your team and what will they be doing?
          title: Team
          placeholder: List the roles and responsibilities of your team members here
          type: textarea
          classes: ismarkdown

        - id: form_stakeholders
          name: stakeholders
          label: What other stakeholders are involved?
          title: Stakeholders
          placeholder: List your project stakeholders here
          type: textarea
          classes: ismarkdown

        - id: form_risks
          name: risks
          label: What could go wrong in your project and how would you handle it?
          title: Risks
          placeholder: List potential risks and control mechanisms/responses here
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: communication
          text: Project Communications and Visibility

        - id: form_audience
          name: communications
          label: Who is your target audience and why are they important?
          title: Audience
          placeholder: Describe your project target audience here
          type: textarea
          classes: ismarkdown

        - id: form_communicationobjectives
          name: communicationobjectives
          label: What are your communications objectives?
          title: Communication Objectives
          placeholder: Describe your project communication plan here
          type: textarea
          classes: ismarkdown

        - id: form_channels
          name: channels
          label: What communications channels will you use?
          title: Channels
          placeholder: Enter your project communication channels here
          type: textarea
          classes: ismarkdown

        - type: spacer
          id: monitoring
          text: Project Monitoring, Reporting and Evaluation

        - id: form_monitor
          name: monitor
          label: How will you monitor your project?
          title: Monitoring
          placeholder: Describe your project monitoring plan here
          type: textarea
          classes: ismarkdown

        - id: form_reports
          name: reports
          label: Do the donors require reports?
          title: Reporting
          placeholder: Describe your project reporting plan here
          type: textarea
          classes: ismarkdown

        - id: form_evaluation
          name: evaluation
          label: How will you document and evaluate the project’s results?
          title: Evaluation
          placeholder: Define how you will evaluate your project here
          type: textarea
          classes: ismarkdown

        - id: form_lessons
          name: lessons
          label: Have you identified 5 lessons learned and 5 recommendations?
          title: Lessons
          placeholder: Include 5 lessons learned and 5 recommendations
          type: textarea
          classes: ismarkdown

    buttons:
        - type: submit
          value: Export Data
          task: plan-form.exportyaml
          icon: fa-download
        - type: submit
          value: Create PDF
          task: plan-form.exportpdf
          icon: fa-file-pdf-o

    process:
        - exportpdf:
            active: true


---
# My Project Plan
