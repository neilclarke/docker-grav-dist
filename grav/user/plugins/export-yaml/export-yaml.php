<?php
namespace Grav\Plugin;

use Grav\Common\Plugin;
use Grav\Common\Utils;
use RocketTheme\Toolbox\File\File;
use RocketTheme\Toolbox\Event\Event;

/**
 * Class ExportYamlPlugin
 * @package Grav\Plugin
 */
class ExportYamlPlugin extends Plugin
{
    /**
     * @return array
     *
     * The getSubscribedEvents() gives the core a list of events
     *     that the plugin wants to listen to. The key of each
     *     array section is the event that the plugin listens to
     *     and the value (in the form of an array) contains the
     *     callable (or function) as well as the priority. The
     *     higher the number the higher the priority.
     */
    public static function getSubscribedEvents()
    {
        return [
            'onPluginsInitialized' => ['onPluginsInitialized', 0],
            'onFormProcessed' => ['onFormProcessed', 0]
        ];
    }

    /**
     * Initialize the plugin
     */
    public function onPluginsInitialized()
    {
        // Don't proceed if we are in the admin plugin
        if ($this->isAdmin()) {
            return;
        }

        // // Enable the main event we are interested in
        // $this->enable([
        //     'onPageContentRaw' => ['onPageContentRaw', 0]
        // ]);


    }

    // /**
    //  * Do some work for this event, full details of events can be found
    //  * on the learn site: http://learn.getgrav.org/plugins/event-hooks
    //  *
    //  * @param Event $e
    //  */
    // public function onPageContentRaw(Event $e)
    // {
    //     // Get a variable from the plugin configuration
    //     $text = $this->grav['config']->get('plugins.export-pdf.text_var');

    //     // Get the current raw content
    //     $content = $e['page']->getRawContent();

    //     // Prepend the output with the custom text and set back on the page
    //     $e['page']->setRawContent($text . "\n\n" . $content);
    // }


    //https://learn.getgrav.org/forms/forms/reference-form-actions#add-your-own-custom-processing-to-a-form
    public function onFormProcessed(Event $event)
    {
        $form = $event['form'];
        $action = $event['action'];
        $params = $event['params'];

        $task = !empty($_POST['task']) ? $_POST['task'] : $uri->param('task');
        if($task && $task === 'plan-form.exportyaml'){

            /** @var Twig $twig */
                $twig = $this->grav['twig'];
                $vars = [
                    'form' => $form
                ];
                $body = $twig->processString('{% include "forms/data.yaml.twig" %}', $vars);

                $filename = 'ProjectPlan-' . date('Ymd-h:i') . '.txt';

                header('Content-type: text/x-yaml');
                header('Content-Disposition: attachment; filename='.$filename);
                /*
                assign file content to a PHP Variable $content
                */
                echo $body;

                exit;
        } 
    }

    /**
     * Create unix timestamp for storing the data into the filesystem.
     *
     * @param string $format
     * @param int    $utimestamp
     *
     * @return string
     */
    private function udate($format = 'u', $utimestamp = null)
    {
        if (is_null($utimestamp)) {
            $utimestamp = microtime(true);
        }

        $timestamp = floor($utimestamp);
        $milliseconds = round(($utimestamp - $timestamp) * 1000000);

        return date(preg_replace('`(?<!\\\\)u`', \sprintf('%06d', $milliseconds), $format), $timestamp);
    }
}
