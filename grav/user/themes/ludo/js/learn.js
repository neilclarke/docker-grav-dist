var yamlFile;

// for the window resize
$(window).resize(function() {

     if($('body').hasClass('guideplanner')){
        setAbsoluteFormButtonPosition();
     }

});

// debouncing function from John Hann
// http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
(function($, sr) {

    var debounce = function(func, threshold, execAsap) {
        var timeout;

        return function debounced() {
            var obj = this, args = arguments;

            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    }
    // smartresize
    jQuery.fn[sr] = function(fn) { return fn ? this.bind('resize', debounce(fn)) : this.trigger(sr); };

})(jQuery, 'smartresize');


jQuery(document).ready(function() { 

});

jQuery(window).on('load', function() {

    jQuery(window).smartresize(function() {
        
    });

    // store this page in session
    sessionStorage.setItem(jQuery('body').data('url'), 1);

    // loop through the sessionStorage and see if something should be marked as visited
    for (var url in sessionStorage) {
        if (sessionStorage.getItem(url) == 1) jQuery('[data-nav-id="' + url + '"]').addClass('visited');
    }

    
    // === LUDOPOLI

    setupMobileMenuToggleHandler();

    setupProjectPlanFormInputs();

    setupProjectPlanInlineFields();

    // Auto-size the text areas
    autosize(document.querySelectorAll('textarea'));

    setupSidePanels();

    setupImportProjectHandlers();

    setupFixedPhaseMenu();

    setupPhaseMenuLinkHandler();

    setupProjectFormEditLinkHandler();

    setupProjectFormAbsoluteButtonHandler();

    setupPhaseMenuExpandOnHoverHandler();
});

function setupMobileMenuToggleHandler(){
    // Mobile Nav Menu Toggle
    $('#main-menu-toggle').on('click',function(e){

        $toggle = $(e.currentTarget);

        var $nav = $('#main-menu ul');
        if($nav.hasClass('expanded')){
            // collapse
            $nav.removeClass('expanded');
            $nav.slideUp();

            $toggle.removeClass('active');
        }
        else {
            // expand
            $nav.addClass('expanded');
            $nav.slideDown();

            $toggle.addClass('active');

            anchor = $('a', $nav)[0];
            anchor.focus();
        }

    });
}
function setupProjectPlanFormInputs(){

    // Project Plan form
    jQuery('form#plan-form input, form#plan-form textarea').on('input', function() {
        var input = jQuery(this),
            value = input.val();

        var sessionVarName = input.attr('name');

        markRelatedFormLinkAfterTextEntry(input);

        if (!value.length) {

            localStorage.removeItem(sessionVarName);
            return;
        }

        localStorage.setItem(sessionVarName, value);

    });
    jQuery('form#plan-form input, form#plan-form textarea').each(function(index,elem){

            var $input = $(elem),
            sessionVarName = $input.attr('name');

            if (localStorage.getItem(sessionVarName)) {
                $input.val(localStorage.getItem(sessionVarName));
                $input.trigger('input');
            }

            // Set our textareas to be read only
            $input.attr('readonly', 'readonly');

            // Convert our textareas to SimpleMDE editors
            if(elem.tagName.toLowerCase() === 'textarea'){
                if($input.hasClass('ismarkdown')){

                    // var mde = new SimpleMDE({ 
                    //     element: elem,
                    //     toolbar: false,
                    //     status: false,
                    //     placeholder: ' '
                    // });

                    // //! This is important. Bind the mde instance to a data attribute on our field.
                    // // We use this for our inline fields
                    // $input.data('MdeInstance', mde);

                    // SimpleMDE.togglePreview(mde);

                    // // Custom change event to trigger the session store function
                    // // This should be happending automatically, but isn't..
                    // mde.codemirror.on("change", function(e){

                    //     var elem = mde.element;
                    //     var source = $(elem);

                    //     SimpleMDE.togglePreview(mde);

                    //     source.val(mde.value());
                    //     source.trigger('input');
                    // });
                }
            }
    });
}
function setupProjectPlanInlineFields(){
    $('data').each(function(index,elem){

        $e = $(elem);
        if($e.hasClass('js-inlinefield')){
            createProjectPlanInlineField($e);
        }

        $e.remove();
    });
}
function setupSidePanels(){
    // View Switches Button
    $('button.viewswitch').on('click',function(e){

        e.preventDefault();

        $btn = $(e.currentTarget);
        var mode = $btn.data('mode');

        switchViewMode(mode);
    });
    //close the lateral panel
    $('.cd-panel').on('click', function(event){
        if( $(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close') ) { 
            switchViewMode('guideonly');
            event.preventDefault();
        }
    });
}
function setupImportProjectHandlers(){

    // Import Project Button
    $('button.import').on('click',function(e){
        $btn = $(e.currentTarget);
        $('#import-project-yaml').show();
    });

    // Import Form YAML
    $('button.imporyaml').on('click', function(e){

    });
     // Set an event listener on the Choose File field.
    $('#yaml-fileselect').on("change", function(e) {
      var files = e.target.files || e.dataTransfer.files;
      // Our file var now holds the selected file
      yamlFile = files[0];
    });

    $('#yaml-uploadbutton').click(function(e) {
      
        e.preventDefault();

        $('#yaml-upload-result').children().remove();

        var reader = new FileReader();

        reader.onload = (function(e){

            return function(e){
                var fileString = e.target.result;
                // For some reason the yaml parser doesn't like \n new line characters
                // We convert them to the break tag for processing. Then convert back to
                // new lines after processing
                var fileWithBr = fileString.replace(/(?:\n)/g, '<br/>');

                var obj =
                processImportedYamlString(fileWithBr);


                var output = '';
                for (var prop in obj) {
                    output += prop + ': ' + obj[prop] + '\r\n';
                }
                $('#yaml-upload-result').text(output);

                // finally resize all textareas
                //autosize(document.querySelectorAll('textarea'));
            }

        })(yamlFile);
        reader.readAsText(yamlFile);

    });
}
function setupPhaseMenuLinkHandler(){
    $('#phase-menu-list a').on('click', function(e){

        $('#side-menu-btn_close').trigger('click');

        var a = e.currentTarget;
        // a is a literal js anchor element, which is the variable 'this' in an anchor click event 
        if (location.pathname.replace(/^\//,'') == a.pathname.replace(/^\//,'') && location.hostname == a.hostname) {

             // We are on the same page. Do Smooth Scroll.
              smoothScrollLink(a);
              return false;
        }
    });
}
function setupProjectFormEditLinkHandler(){
    $('#plan-form a.field-edit-link-anchor').on('click', function(e){
        $('#navbar-btn_view_guide').trigger('click');

        var a = e.currentTarget;

        // a is a literal js anchor element, which is the variable 'this' in an anchor click event 
        if (location.pathname.replace(/^\//,'') == a.pathname.replace(/^\//,'') && location.hostname == a.hostname) {

              smoothScrollLink(a);

              // focus on the textarea
              var textareaid = $(e.currentTarget).data('textarea');
              $(textareaid).focus();

              return false;
        }

    });
}
function setupProjectFormAbsoluteButtonHandler(){
    $('#absolute-form-buttons button').on('click', function(e){


        var target = $(e.currentTarget).data('target');
        if(target){
            var formBtn = $('button[value="'+ target + '"]');
            formBtn.click();

            e.preventDefault();
        }

    });
}
function setupPhaseMenuExpandOnHoverHandler(){
    //= UYF-73. 
    //# Expand phase menu list items on hover.
    $('#phase-menu-list > li > a').on({
        mouseenter: function (e) {
            $parent = $(e.currentTarget).parent();
            $list = $(e.currentTarget).next('ul.phase-menu-children');

            if ($list.is(':visible')){
                $list.delay(750).slideUp();
            }
            else {
                $list.delay(250).slideDown();
            }
        }
    });
    // keyboard support
    $('#phase-menu-list > li > a').on({
        focus: function(e) {
            $parent = $(e.currentTarget).parent();
            $parent.find('ul.phase-menu-children').slideDown();
        },
        focusout: function(e) {
            $parent = $(e.currentTarget);
            if(!($parent.hasClass('active'))){
                $parent.find('ul.phase-menu-children').slideUp('slow');
            }

        }
    });

}
function setupFixedPhaseMenu(){

    // find our header menu
    var header = document.getElementById('header');
    // our fixed position indicator is height of the header
    var fixedPositionToBegin = header.offsetHeight;

    // if our document scroll has gone past the bottom of header menu, make phase menu fixed.
    if ($(document).scrollTop() > fixedPositionToBegin) {
        $('body').addClass("fixed-phase-menubar");
    } else {
        $('body').removeClass("fixed-phase-menubar");
    }

    $(window).scroll( $.throttle( 250, checkFixedHeaderPosition ) );
}
//
//== end setup functions
    


var fixedPositionToBegin = 120;
var isPhaseMenuFixed = false;
function checkFixedHeaderPosition(){

    if ($(window).scrollTop() > fixedPositionToBegin) {
            if(!isPhaseMenuFixed){
                $('body').addClass("fixed-phase-menubar");
                isPhaseMenuFixed = true;
            }
        } else {
            if(isPhaseMenuFixed){
                $('body').removeClass("fixed-phase-menubar");
                isPhaseMenuFixed = false;
            }
    }
}

function smoothScrollLink(a){
    var target = $(a.hash);
    target = target.length ? target : $('[name=' + a.hash.slice(1) +']');
    smoothScrollToElement(target);
}

function smoothScrollToElement(target){
    if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top - 60
        }, 800);
    }
}

function processImportedYamlString(yamlString){

    var yaml = jsyaml.load(yamlString);


    for (var prop in yaml) {

        var sessionVarName = 'data[' + prop + ']';

        var text = '';

        if(yaml[prop] !== null){
            text = yaml[prop];
        }
        // blat the blank space at the beginning if there is one
        text = text.replace('   ', '');
        // now convert our breaks back to new lines, see the file read
        text = text.split(" <br/>").join("\n");
        text = text.split("<br/>").join("\n");


        // first try and find an inline field
        var inline = $('#form_'+prop+'-inline');
        if(inline && inline !== undefined && inline.length > 0) {
            // triggering the inline change will also trigger the actual form field
            inline.val(text).trigger('input');
        }
        // there's no inline field. find the actual form field.
        else {
            $('#form_'+prop).val(text).trigger('input');
        }

        autosize.update(document.querySelectorAll('textarea'));
    }


    return yaml;

}

// Project Planner : Inline Fields
function createProjectPlanInlineField(element){

    var $elem = $(element);
    if($elem !== undefined && $elem.hasClass('js-inlinefield')){

        var fieldId = $elem.attr('value');
        if(fieldId && fieldId.length > 0){
            // get the field from the actual form
            var field = document.getElementById(fieldId);
            if(field && field !== undefined){
                // basically just clone it.
                var $clone = $(field).clone();

                // Resolve the IE11 bug. Where the placeholder get
                // turned into the actual value of the textarea
                sessionVarName = $clone.attr('name');
                var textareavalue = '';
                if (localStorage.getItem(sessionVarName)) {
                    textareavalue = localStorage.getItem(sessionVarName);
                }
                $clone.val(textareavalue);

                $clone.appendTo(($elem).parent());

                if($clone && $clone !== undefined){

                    $clone
                    .attr('id', fieldId + '-inline')
                    .attr('rows', '6')
                    .prop('readonly', false)
                    .data('sourcefield', fieldId);

                    // Rich Text Fields
                    //// this function will create a markdown editor for the text field
                    //createMarkdownEditorForInlineField($clone, field);

                    // Simple Text Fields
                    createSynchedTextAreaForInlineField($clone, field);

                }
                else {
                    console.error('could not clone textarea for js-inlinefield');
                }
                
                
            }
            else {
                console.warn('cannot find related field for js-inlinefield. value = ' + fieldId);
            }
        }

    }

}

function createMarkdownEditorForInlineField(inlineField, sourceField) {

    var $inline = $(inlineField),
        $source = $(sourceField);

    if($inline && $inline !== undefined){

                    // $inline
                    // .attr('id', fieldId + '-inline')
                    // .data('sourcefield', fieldId);

                    // create new mde editor
                    var inlineMde = convertTextareaToMarkdownEditor($inline[0]);

                    // get the source doc and link it, 
                    // see: http://codemirror.net/doc/manual.html#api_doc

                    // pull related mde from data attribute on input field
                    var sourceMde = $source.data('MdeInstance');
                    if(sourceMde !== undefined){
                        var sourceDoc = sourceMde.codemirror.getDoc();
                        var newDoc = sourceDoc.linkedDoc({'sharedHist':true});
                        inlineMde.codemirror.swapDoc(newDoc);
                    }
                    else {
                        console.warn('souceMde instance is undefined');
                    }
                }
     else {
        console.error('inlineField is not defined | createMarkdownEditorForInlineField');
     }

}

function convertTextareaToMarkdownEditor(textarea){
    return new SimpleMDE({ 
                element: textarea,
                forceSync: true,
                toolbar: ["heading", "|", "bold", "italic", "|", "unordered-list", "|", "preview"]
            });
}

function createSynchedTextAreaForInlineField(inlineField, sourceField){
    // this function takes a simple text area for an inline field
    // and creates hooks to synchronise it with it's related form field

    inlineField = $(inlineField);
    sourceField = $(sourceField);

    inlineField.on('input', function(e){
        var text = e.currentTarget;

        var noQuotes = (text.value).replace(/"/g, "'");

        sourceField.val(noQuotes);
        sourceField.trigger('input');
    });

    sourceField.attr('readonly', 'readonly');

}

function switchViewMode(mode){
    //modes=
    //  guide-planner
    //  guideonly
    //  planneronly

    $body = $('body'),
    $html = $('html');

    var switchClasses = 'guideonly guideplanner planneronly side-menu side-menu-only'
    
    switch(mode){
        case 'guideplanner':
            $body.removeClass(switchClasses);
            $html.removeClass(switchClasses);
            $body.addClass('guideplanner');
            $html.addClass('guideplanner');

            $('#plan-form textarea').each(function(e){
                $e = $(e.currentTarget);
                $('a.fa-eye', $e).click();
            });

            setTimeout(function(){ $('button#navbar-btn_view_guide-top').focus(); }, 200);

            $('#aside-preview.cd-panel').addClass('is-visible');
            // autosize form textareas
            autosize.update(document.querySelectorAll('textarea'));
            // calculated absolute form button width
            setAbsoluteFormButtonPosition();

            $('#aside-preview').css('overflow-y', 'visible');

            break;
        case 'guideonly':
            $body.removeClass(switchClasses);
            $body.addClass('guideonly');
            $html.removeClass(switchClasses);
            $html.addClass('guideonly');
            $('.cd-panel').removeClass('is-visible');
            break;
        case 'planneronly':
            $body.removeClass(switchClasses);
            $body.addClass('planneronly');
            $html.removeClass(switchClasses);
            $html.addClass('planneronly');
            break;
        case 'side-menu':
            $body.removeClass(switchClasses);
            $body.addClass('side-menu');
            $html.removeClass(switchClasses);
            $html.addClass('side-menu');

            // give focus to the menu - after a timeout. This is because of a safari rendering issue.
            setTimeout(function(){ $('#side-menu-btn_close').focus(); }, 200);

            $('#side-menu.cd-panel').addClass('is-visible');
            
            break;
        case 'side-menu-only':
            $body.removeClass(switchClasses);
            $body.addClass('side-menu-only');
            $html.removeClass(switchClasses);
            $html.addClass('side-menu-only');
            break;
        default:
            break;
    }
}
function setAbsoluteFormButtonPosition(){
    // calculated absolute form button width
    $('#absolute-form-buttons').width($('#aside-preview .cd-panel-container').width());
    $('#absolute-form-buttons').css('top', (window.innerHeight + window.pageYOffset-70));
}

// UYF-74
// Phase menu: make bullet of question green after text entry
function markRelatedFormLinkAfterTextEntry($textarea){
    // WARN: This function has no checks for speed. Make sure you pass in
    // a correct jquery object

    // we get our textarea id and look for an anchor with that id plus '-link'
    // appended to it. See phase-menu.html.twig for where this is currently defined
    var id = $textarea.attr('id') + '-link'; 
    if($textarea.val().length > 0){
        $('#'+id).addClass('completed');
    }
    else {
        $('#'+id).removeClass('completed');
    }
}


$(function() {
    $('a[rel="lightbox"]').featherlight({
        root: 'section#body'
    });
}); 
